$(function () {

    $(".hide-menu").click(function () {
//alert( "Handler for .click() called." );
        $("body").toggleClass("hide-sidebar");
    });
    $(".has-scroll").mCustomScrollbar({
        setWidth: false,
        setHeight: false,
        setTop: 0,
        setLeft: 0,
        axis: "y",
        scrollbarPosition: "inside",
        scrollInertia: 150,
        autoDraggerLength: true,
        autoHideScrollbar: true,
        autoExpandScrollbar: false,
        alwaysShowScrollbar: 0,
        snapAmount: null,
        snapOffset: 0,
        mouseWheel: {
            enable: true,
            scrollAmount: "auto",
            axis: "y",
            preventDefault: false,
            deltaFactor: "auto",
            normalizeDelta: false,
            invert: false,
            disableOver: ["select", "option", "keygen", "datalist", "textarea"]
        },
        contentTouchScroll: 5,
        theme: "dark-3"
    });
});
function getStategiesByGroupEnv(groupId, envId) {

    var data = {groupId: groupId, environmentId: envId};
    //alert(JSON.stringify(data));

    $.ajax({
        type: "POST",
        url: "/karma/groups/strategies",
        data: JSON.stringify(data),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: true,
        success: function (data)
        {

            $("#ulstrategies").empty();
            var strHeader = '<li class="list-group-item">';
            strHeader += '<ul class="actions pull-right">';
            strHeader += '<li><a href=""><i class="zmdi zmdi-play"></i></a></li>';
            strHeader += '<li><a href=""><i class="zmdi zmdi-edit"></i></a></li>';
            strHeader += '<li><a href=""><i class="zmdi zmdi-refresh"></i></a></li>';
            var item;
            $.each(data, function (key, value) {


                item = strHeader + '</ul><a href="#">' + value.guiname + '</a></li>';
                $("#ulstrategies").append(item);
                item = '';
            })
        },
        error: function (e)
        {
            alert("ERROR: ", e);
        }
    });
}

function renderStrategies(data)
{

    $("#ulstrategies").empty();
    var strHeader = '<li class="list-group-item">';
    strHeader += '<ul class="actions pull-right">';
    strHeader += '<li><a href=""><i class="zmdi zmdi-play"></i></a></li>';
    strHeader += '<li><a href=""><i class="zmdi zmdi-edit"></i></a></li>';
    strHeader += '<li><a href=""><i class="zmdi zmdi-refresh"></i></a></li>';
    var item;
    $.each(data, function (key, value) {


        item = strHeader + '</ul><a href="#">' + value.guiname + '</a></li>';
        $("#ulstrategies").append(item);
        item = '';
    })
}

function refreshStrategies(source, event, render) {
    jsf.ajax.request(source, event, {render: render});
}

function buildTree(id, data) {

    $('#' + id).jstree({
        "json_data": {
            "data": [data]
        },
        "themes": {
            "theme": "apple"
        },
        "plugins": ["themes", "json_data", "ui"]
    }).bind("select_node.jstree", function (e, data) {
        alert(data.rslt.obj.attr("id"));
    });
}


