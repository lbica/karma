package com.irealsoftware.karma.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-06-20T17:08:19")
@StaticMetamodel(DataItemTop.class)
public class DataItemTop_ { 

    public static volatile SingularAttribute<DataItemTop, String> topType;
    public static volatile SingularAttribute<DataItemTop, String> dataOrder;
    public static volatile SingularAttribute<DataItemTop, String> topTiesType;
    public static volatile SingularAttribute<DataItemTop, Integer> environmentId;
    public static volatile SingularAttribute<DataItemTop, String> nullInclude;
    public static volatile SingularAttribute<DataItemTop, Integer> topCount;
    public static volatile SingularAttribute<DataItemTop, String> nullOrder;
    public static volatile SingularAttribute<DataItemTop, Integer> groupItemNodeId;

}