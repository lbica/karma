package com.irealsoftware.karma.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-06-20T17:08:19")
@StaticMetamodel(NodeListItem.class)
public class NodeListItem_ { 

    public static volatile SingularAttribute<NodeListItem, String> guiName;
    public static volatile SingularAttribute<NodeListItem, Integer> id;
    public static volatile SingularAttribute<NodeListItem, String> guiDescription;

}