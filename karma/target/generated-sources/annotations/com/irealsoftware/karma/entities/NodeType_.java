package com.irealsoftware.karma.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-06-20T17:08:19")
@StaticMetamodel(NodeType.class)
public class NodeType_ { 

    public static volatile SingularAttribute<NodeType, String> guiName;
    public static volatile SingularAttribute<NodeType, String> operationalName;
    public static volatile SingularAttribute<NodeType, Integer> id;

}