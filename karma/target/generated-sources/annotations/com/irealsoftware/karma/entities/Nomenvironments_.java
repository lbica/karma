package com.irealsoftware.karma.entities;

import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-06-20T17:08:19")
@StaticMetamodel(Nomenvironments.class)
public class Nomenvironments_ { 

    public static volatile SingularAttribute<Nomenvironments, String> operationalschema;
    public static volatile SingularAttribute<Nomenvironments, String> sysUserHost;
    public static volatile SingularAttribute<Nomenvironments, String> guiname;
    public static volatile SingularAttribute<Nomenvironments, String> guidescription;
    public static volatile SingularAttribute<Nomenvironments, BigInteger> environmentgroupid;
    public static volatile SingularAttribute<Nomenvironments, Date> sysUpdateDate;
    public static volatile SingularAttribute<Nomenvironments, String> sysUserOs;
    public static volatile SingularAttribute<Nomenvironments, Short> isproduction;
    public static volatile SingularAttribute<Nomenvironments, Date> sysCreationDate;
    public static volatile SingularAttribute<Nomenvironments, String> sysUserApp;
    public static volatile SingularAttribute<Nomenvironments, BigInteger> sysSeqId;
    public static volatile SingularAttribute<Nomenvironments, Integer> id;
    public static volatile SingularAttribute<Nomenvironments, String> sysUserProgram;
    public static volatile SingularAttribute<Nomenvironments, Character> operationalstatus;
    public static volatile SingularAttribute<Nomenvironments, String> sysUserDb;

}