package com.irealsoftware.karma.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-06-20T17:08:19")
@StaticMetamodel(StrategyNode.class)
public class StrategyNode_ { 

    public static volatile SingularAttribute<StrategyNode, String> operationalStatus;
    public static volatile SingularAttribute<StrategyNode, String> guiText;
    public static volatile SingularAttribute<StrategyNode, Integer> environmentId;
    public static volatile SingularAttribute<StrategyNode, Integer> strategyId;
    public static volatile SingularAttribute<StrategyNode, Integer> id;
    public static volatile SingularAttribute<StrategyNode, String> type;
    public static volatile SingularAttribute<StrategyNode, Integer> projectId;
    public static volatile SingularAttribute<StrategyNode, Integer> parentId;
    public static volatile SingularAttribute<StrategyNode, Integer> priorityId;

}