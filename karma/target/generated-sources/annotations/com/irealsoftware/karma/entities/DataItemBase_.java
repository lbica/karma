package com.irealsoftware.karma.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-06-20T17:08:19")
@StaticMetamodel(DataItemBase.class)
public class DataItemBase_ { 

    public static volatile SingularAttribute<DataItemBase, String> expressionValue;
    public static volatile SingularAttribute<DataItemBase, String> guiName;
    public static volatile SingularAttribute<DataItemBase, String> operationalName;
    public static volatile SingularAttribute<DataItemBase, Integer> id;
    public static volatile SingularAttribute<DataItemBase, String> guiDescription;

}