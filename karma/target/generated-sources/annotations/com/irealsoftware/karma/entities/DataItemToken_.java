package com.irealsoftware.karma.entities;

import com.irealsoftware.karma.entities.TokenTypeEnum;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-06-20T17:08:19")
@StaticMetamodel(DataItemToken.class)
public class DataItemToken_ { 

    public static volatile SingularAttribute<DataItemToken, String> tokenSetName;
    public static volatile SingularAttribute<DataItemToken, String> tokenText;
    public static volatile SingularAttribute<DataItemToken, String> guiControlName;
    public static volatile SingularAttribute<DataItemToken, Integer> tokenRefId;
    public static volatile SingularAttribute<DataItemToken, TokenTypeEnum> tokenType;
    public static volatile SingularAttribute<DataItemToken, Boolean> isEligibleForSave;

}