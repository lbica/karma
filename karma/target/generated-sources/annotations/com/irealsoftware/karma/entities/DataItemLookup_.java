package com.irealsoftware.karma.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-06-20T17:08:19")
@StaticMetamodel(DataItemLookup.class)
public class DataItemLookup_ { 

    public static volatile SingularAttribute<DataItemLookup, String> groupItemValue;
    public static volatile SingularAttribute<DataItemLookup, String> role;
    public static volatile SingularAttribute<DataItemLookup, Integer> dataTableId;
    public static volatile SingularAttribute<DataItemLookup, Integer> dataTableFieldId;
    public static volatile SingularAttribute<DataItemLookup, Integer> groupItemElementId;

}