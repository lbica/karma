/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.irealsoftware.karma.facade;

import com.irealsoftware.karma.entities.Nomenvironments;
import java.io.File;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.Context;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Laurentiu
 */
public class NomenvironmentsFacadeTest {

    public NomenvironmentsFacadeTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
    }

    //@org.junit.Test
    public void testGetConnection() throws Exception {
        System.out.println("testGetConnection");

        Map properties = new HashMap();
        //properties.put(EJBContainer.APP_NAME, "karma");
        properties.put(EJBContainer.MODULES, new File[]{new File("target/classes")});
        properties.put(EJBContainer.PROVIDER, "org.glassfish.ejb.embedded.EJBContainerProviderImpl");

        EJBContainer container = javax.ejb.embeddable.EJBContainer.createEJBContainer(properties);

        DataSource ds = (DataSource) container.getContext().lookup("jdbc/karmaDatasource");

        Connection conn = ds.getConnection();

        System.out.println(conn.getMetaData().getDatabaseProductName());

    }

    //@org.junit.Test
    public void testGetSandboxNomenvironments() throws Exception {
        System.out.println("getSandboxNomenvironments");

//        Map properties = new HashMap();
//        properties.put(EJBContainer.APP_NAME, "karma");
//        properties.put(EJBContainer.MODULES, new File[]{new File("target/classes")});
//        properties.put(EJBContainer.PROVIDER, "org.glassfish.ejb.embedded.EJBContainerProviderImpl");
        Map props = new HashMap();
        props.put("org.glassfish.ejb.embedded.glassfish.configuration.file", "setup/glassfish-resources.xml");

        EJBContainer ejbC = EJBContainer.createEJBContainer(props);
        Context ctx = ejbC.getContext();
        DAOFacade instance = (DAOFacade) ctx.lookup("java:global/classes/NomenvironmentsFacade");

        Nomenvironments result = instance.getSandboxNomenvironments();
        assertNotNull(result);
        ejbC.close();
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    //@Test
    public void testEJB() throws NamingException {
        EJBContainer ejbC = EJBContainer.createEJBContainer();
        Context ctx = ejbC.getContext();
        DAOFacade app = (DAOFacade) ctx.lookup("java:global/classes/NomenvironmentsFacade");
        assertNotNull(app);
//        String NAME = "Duke";
//        String greeting = app.sayHello(NAME);
//        assertNotNull(greeting);
//        assertTrue(greeting.equals("Hello " + NAME));
        ejbC.close();
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
