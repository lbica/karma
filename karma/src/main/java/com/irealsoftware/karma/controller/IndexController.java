package com.irealsoftware.karma.controller;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import static com.google.common.collect.Iterables.filter;
import com.google.common.collect.Lists;
import com.irealsoftware.karma.customcomponents.AbstractUINodeComponent;
import com.irealsoftware.karma.customcomponents.UINodeAssign;
import com.irealsoftware.karma.customcomponents.UINodeCallStrategy;
import com.irealsoftware.karma.customcomponents.UINodeFilter;
import com.irealsoftware.karma.customcomponents.UINodeFilterOther;
import com.irealsoftware.karma.customcomponents.UINodeLookupGet;
import com.irealsoftware.karma.customcomponents.UINodeLookupSet;
import com.irealsoftware.karma.customcomponents.UINodeStrategyDefinition;
import com.irealsoftware.karma.entities.GenericNode;
import com.irealsoftware.karma.entities.NodeHierarchy;
import com.irealsoftware.karma.entities.NodeHierarchyType;
import com.irealsoftware.karma.entities.NodeType;
import com.irealsoftware.karma.entities.Nomenvironments;
import com.irealsoftware.karma.entities.OperationType;
import com.irealsoftware.karma.entities.PasteAction;
import com.irealsoftware.karma.entities.Project;
import com.irealsoftware.karma.entities.Strategy;
import com.irealsoftware.karma.entities.StrategyNode;
import com.irealsoftware.karma.facade.DAOFacade;
import java.util.List;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;
import java.util.Objects;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIPanel;
import javax.faces.component.html.HtmlPanelGroup;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;
import org.richfaces.component.AbstractTree;
import org.richfaces.event.DropEvent;
import org.richfaces.event.TreeSelectionChangeEvent;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.filter;
import static com.google.common.collect.Iterables.filter;

/**
 *
 * @author juneau
 */
@ManagedBean(name = "indexController")
@SessionScoped
public class IndexController implements Serializable {

    public final String MENU_ITEM_CURRENTNODE = "miCurrentNode";

    public final String MENU_ITEM_CHILDNODES = "miChildNodes";

    public final String MENU_ITEM_ALLNODES = "miAllNodes";

    // <editor-fold defaultstate="collapsed" desc="Fields">
//    @EJB
//    private NomenvironmentsFacade ejbFacade;
    @EJB
    private DAOFacade daoFacade;

    private Integer currentEnvId = null;

    private Project currentProject = null;

    private Strategy currentStrategy = null;

    private static boolean isFirstLoad = true;

    private NodeHierarchy rootNode;

    private NodeHierarchy currentSelection = null;

    private NodeHierarchy currentCopySelection = null;

    private NodeHierarchy currentPasteSelection = null;

    private boolean copyExecuted = false;

    private OperationType currentOperationType = OperationType.None;

    private UIPanel placeHolder = new HtmlPanelGroup();

    private UIPanel panelAdd = new HtmlPanelGroup();

    public UIPanel getPanelAdd() {
        return panelAdd;
    }

    public void setPanelAdd(UIPanel panelAdd) {
        this.panelAdd = panelAdd;
    }

    private List<Strategy> strategiesList = null;

    private List<StrategyNode> strategiesNodes = null;

    private List<GenericNode> treeRoots = null;

    private List<NodeType> nodeTypeList;

    public List<NodeType> getNodeTypeList() {
        return nodeTypeList;
    }

    public UIPanel getPlaceHolder() {
        return placeHolder;
    }

    public void setPlaceHolder(UIPanel placeHolder) {
        this.placeHolder = placeHolder;
    }

    public OperationType getCurrentOperationType() {
        return currentOperationType;
    }

    public void setCurrentOperationType(OperationType currentOperationType) {
        this.currentOperationType = currentOperationType;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Instance Constructors">
    public IndexController() {
        //authorLast = null;
    }

    // </editor-fold>
    @PostConstruct
    public void init() {
        this.loadDefaults();

        //this.buildTree(this.getCurrentEnvId(), this.getCurrentProject().getId(), this.getCurrentStrategy().getId());
    }

    public NodeHierarchy getRootNode() {

        return rootNode;
    }

    //get first enid(the production)
    public Integer getCurrentEnvId() {

        if (this.currentEnvId == null) {
            this.currentEnvId = daoFacade.getProdNomenvironments().getId();
        }

        return this.currentEnvId;
    }

    /*
    //get the first project id based on envid
     */
    public Project getCurrentProject() {

        if (this.currentProject == null) {
            this.currentProject = daoFacade.findFirstProjectByEnv(getCurrentEnvId());
        }

        return currentProject;
    }

    /*
    //get the first strategy id based on envid and projectid
     */
    public Strategy getCurrentStrategy() {

        if (this.currentStrategy == null) {
            this.currentStrategy = daoFacade.findFirstStrategyByEnvProject(getCurrentEnvId(), getCurrentProject().getId());
        }
        return this.currentStrategy;
    }

//    @EJB
//    private AuthorWorkType authorWorkFacade;
//    
    public List<StrategyNode> getStrategiesNodes() {
        return strategiesNodes;
    }

    public List<Strategy> getStrategiesList() {
        return this.strategiesList;
    }

    public Nomenvironments getSandboxNomenvironments() {
        return daoFacade.getSandboxNomenvironments();
    }

    public Nomenvironments getProdNomenvironments() {
        return daoFacade.getProdNomenvironments();
    }

    public List<Map> loadProjectsByEnv(int envId) {
        return this.daoFacade.getProjectsByEnv(envId);
    }

//        public List<Map> findStrategies() {
//        return this.nomenvFacade.findStrategiesByEnvGroup(envId, groupId);
//    }
    // <editor-fold defaultstate="collapsed" desc="Helper Methods">
    private void pasteNode(StrategyNode source, StrategyNode destination, String pasteType) {
        this.daoFacade.pasteOneNode(source.getId(), (destination.getType().equals("GROUPITEM") ? null : destination.getId()),
                this.currentStrategy.getId(), pasteType, this.currentStrategy.getEnvironmentId(), null);

        buildTree(this.currentStrategy.getEnvironmentId(), this.currentStrategy.getId());
    }

    private void loadStrategiesByEnvProject(int envId, int projectId) {
        this.strategiesList = this.daoFacade.getStrategiesByEnvProject(envId, projectId);
    }

    private void loadNodeType(int envId, int projectId) {
        this.nodeTypeList = this.daoFacade.getNodeTypeByEnvProject(envId, projectId);
    }

//    private void loadStrategyNodes(int envId, int groupId, int strategyId) {
//        this.strategiesNodes = this.nomenvFacade.getStrategyNodes(envId, groupId, strategyId);
//
//        //this.buildTree(this.strategiesNodes);
//    }

    /*
    // build the hierarchy for rootNode local field 
    // based on data parameter
     */
    private void buildTree(int environmentId, int strategyId) {

        this.strategiesNodes = this.daoFacade.loadDetailsTree(environmentId, strategyId);

        final Integer startWith = null;

        //find the parent using startWith condition
        StrategyNode parentStrategy = Iterables.find(this.strategiesNodes, new Predicate<StrategyNode>() {
            @Override
            public boolean apply(StrategyNode input) {
                return Objects.equals(startWith, input.getParentId());
            }
        });

        // create Root instance
        this.rootNode = new NodeHierarchy(parentStrategy, NodeHierarchyType.Root);

        //build the nodes recursive
        createNodes(this.strategiesNodes, this.rootNode);

    }

    private void createNodes(List<StrategyNode> data, final NodeHierarchy parent) {

        List<StrategyNode> filteredData = Lists.newArrayList();

        Iterables.addAll(filteredData, filter(data, new Predicate<StrategyNode>() {
            @Override
            public boolean apply(StrategyNode input) {
                return Objects.equals(parent.getId(), input.getParentId());
            }
        }));

        for (StrategyNode item : filteredData) {

            NodeHierarchy leaf = new NodeHierarchy(item, NodeHierarchyType.Leaf);

            parent.addChild(leaf);

            if (parent.getNodeType() != NodeHierarchyType.Root) {
                parent.setNodeType(NodeHierarchyType.Parent);
            }

            createNodes(data, leaf);

        }

    }

    private void showNodeType() {

        StrategyNode sn = this.currentSelection.getData();
        
        AbstractUINodeComponent component = null;

        if (sn == null) {
            return;
        }

        //clear the placeholder's childs 
        this.placeHolder.getChildren().clear();
       
        switch (sn.getType()) {
            case "FILTER":
                component = createUINodeFilter(sn);
                break;

            case "FILTER_OTHER":
                component = createUINodeFilterOther(sn);
                break;

            case "ASSIGN":
                component = createUINodeAssign(sn);
                break;

            case "CALL_STRATEGY":
                component = createUINodeCallStrategy(sn);
                break;

            case "LOOKUP_GET":
                component = createUINodeLookupGet(sn);
                break;

            case "LOOKUP_SET":
                component = createUINodeLookupSet(sn);
                break;

            case "GROUPITEM":
                component = createUINodeStrategyDefinition(sn);
                break;

            default:
                break;
        }
        
        //add the placeholder's child
        if (component != null)
            this.placeHolder.getChildren().add(component);
    }

    private AbstractUINodeComponent createUINodeFilter(StrategyNode data) {
        UINodeFilter filter = new UINodeFilter(data, this.daoFacade);

        if (data != null) {
            String name = data.getType() + "%" + data.getId();

            filter.setName(name);
        }

        return filter;
    }

    private AbstractUINodeComponent createUINodeFilterOther(StrategyNode data) {
        UINodeFilterOther filterOther = new UINodeFilterOther(data, this.daoFacade);

        if (data != null) {
            String name = data.getType() + "%" + data.getId();

            filterOther.setName(name);
        }
        
        return filterOther;

    }

    private AbstractUINodeComponent createUINodeAssign(StrategyNode data) {
        UINodeAssign assign = new UINodeAssign(data, this.daoFacade);

        if (data != null) {
            String name = data.getType() + "%" + data.getId();

            assign.setName(name);
        }

        return assign;
    }

    private AbstractUINodeComponent createUINodeStrategyDefinition(StrategyNode data) {
        UINodeStrategyDefinition strategyDef = new UINodeStrategyDefinition(data, this.daoFacade);

        if (data != null) {
            String name = data.getType() + "%" + data.getId();

            strategyDef.setName(name);
        }

        return strategyDef;
    }

    private AbstractUINodeComponent createUINodeCallStrategy(StrategyNode data) {
        UINodeCallStrategy call_strategy = new UINodeCallStrategy(data, this.daoFacade);

        if (data != null) {
            String name = data.getType() + "%" + data.getId();

            call_strategy.setName(name);
        }

        return call_strategy;
    }

    private AbstractUINodeComponent createUINodeLookupGet(StrategyNode data) {
        UINodeLookupGet lookupGet = new UINodeLookupGet(data, this.daoFacade);

        if (data != null) {
            String name = data.getType() + "%" + data.getId();

            lookupGet.setName(name);
        }

        return lookupGet;
    }

    private AbstractUINodeComponent createUINodeLookupSet(StrategyNode data) {
        UINodeLookupSet lookupSet = new UINodeLookupSet(data, this.daoFacade);

        if (data != null) {
            String name = data.getType() + "%" + data.getId();

            lookupSet.setName(name);
        }

        return lookupSet;
    }

    public synchronized void loadDefaults() {
        //get the production envid
        //this.currentEnvId = nomenvFacade.getProdNomenvironments().getId();

        //get the first group based on envid
        //this.currentGroupId = nomenvFacade.findFirstGroupByNomenvironment(this.CurrentEnvId).getId();
        //get the first groupitem based on envid and groupid
        //this.currentGroupId = nomenvFacade.findFirstStrategyByEnvGroup(this.CurrentEnvId, this.CurrentGroupId).getId();
        loadStrategiesByEnvProject(this.getCurrentEnvId(), this.getCurrentProject().getId());

        buildTree(this.getCurrentEnvId(), this.getCurrentStrategy().getId());

//        this.loadStrategyNodes(this.getCurrentEnvId(), this.getCurrentProject().getId(), this.getCurrentStrategy().getId());
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Event Handlers">
    public void onClickProject(Project project) {

        //set current project
        this.currentProject = project;
        loadStrategiesByEnvProject(project.getEnvironmentId(), project.getId());

    }

    public void onClickStrategy(Strategy strategy) {
        //set current strategy
        this.currentStrategy = strategy;

//        loadStrategyNodes(strategy.getEnvironmentId(), strategy.getProjectId(), strategy.getId());
        buildTree(strategy.getEnvironmentId(), strategy.getId());
    }

    public void processDrop(DropEvent event) {
        NodeHierarchy draggedNode = (NodeHierarchy) event.getDragValue();
        NodeHierarchy droppedNode = (NodeHierarchy) event.getDropValue();
        ((NodeHierarchy) draggedNode.getParent()).removeChild(draggedNode);
        droppedNode.addChild(draggedNode);
    }

    public void childNodeListener(ActionEvent event) {
        ActionEvent ev = event;
        //this.rootNode.
    }

    public void copyNodeListener(ActionEvent event) {
        this.currentCopySelection = this.currentSelection;
    }

    public void pasteNodeListener(ActionEvent event) {

        String id = event.getComponent().getId();

        if (id.equals(MENU_ITEM_CURRENTNODE)) {
            pasteNode(this.currentCopySelection.getData(), this.currentSelection.getData(), PasteAction.NodeOnly.toString());
        } else if (id.equals(MENU_ITEM_CHILDNODES)) {
            pasteNode(this.currentCopySelection.getData(), this.currentSelection.getData(), PasteAction.ChildsOnly.toString());
        } else if (id.equals(MENU_ITEM_ALLNODES)) {
            pasteNode(this.currentCopySelection.getData(), this.currentSelection.getData(), PasteAction.WithChilds.toString());
        }

    }

    public void removeNodeListener(ActionEvent event) {

//        String id = event.getComponent().getId();
        StrategyNode node = this.currentSelection.getData();

        if (node == null) {
            return;
        }

        Integer id = this.currentSelection.getNodeType() == NodeHierarchyType.Root ? null : node.getId();

        this.daoFacade.nodeSave(id, node.getEnvironmentId(), node.getStrategyId(),
                null, null, null, null, null, null, null, null, "DA", null, null);

        buildTree(this.currentStrategy.getEnvironmentId(), this.currentStrategy.getId());

    }

    public void selectionChangeListener(TreeSelectionChangeEvent selectionChangeEvent) {
        // considering only single selection
        List<Object> selection = new ArrayList<Object>(selectionChangeEvent.getNewSelection());
        Object currentSelectionKey = selection.get(0);
        AbstractTree tree = (AbstractTree) selectionChangeEvent.getSource();

        Object storedKey = tree.getRowKey();
        tree.setRowKey(currentSelectionKey);
        this.currentSelection = (NodeHierarchy) tree.getRowData();
        tree.setRowKey(storedKey);

        this.currentOperationType = OperationType.Edit;

        this.panelAdd.setRendered(false);

        showNodeType();
    }

    public void addChildNodeListener() {
        this.currentOperationType = OperationType.Add;

        if (this.nodeTypeList == null) {
            loadNodeType(this.currentEnvId, this.currentProject.getId());
        }

        this.panelAdd.setRendered(true);
        this.placeHolder.getChildren().clear();
    }

    public void changeValueNodeTypeListener(ValueChangeEvent event) {
        ValueChangeEvent ev = event;

        String opName = event.getNewValue().toString();

        this.placeHolder.getChildren().clear();
        switch (opName) {
            case "FILTER":
                createUINodeFilter(null);
                break;

            case "FILTER_OTHER":
                createUINodeFilterOther(null);
                break;

            case "ASSIGN":
                createUINodeAssign(null);
                break;

            case "CALL_STRATEGY":
                createUINodeCallStrategy(null);
                break;

            case "LOOKUP_GET":
                createUINodeLookupGet(null);
                break;

            case "LOOKUP_SET":
                createUINodeLookupSet(null);
                break;

            case "GROUPITEM":
                createUINodeStrategyDefinition(null);
                break;

            default:
                break;
        }

    }

    // </editor-fold>
    public NodeHierarchy getCurrentSelection() {
        return this.currentSelection;
    }

}
