/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.irealsoftware.karma.helpers;

import com.irealsoftware.karma.exceptions.NotSupportedException;
import java.util.AbstractMap.SimpleEntry;
import java.util.Map.Entry;

/**
 *
 * @author Laurentiu
 */
public final class StringHelper {
    
    

    public static Entry<Integer, String> getKeyValueFromParsedString(String input, String sep) throws NotSupportedException {
        //split into 2 items array string separated by semicolon
        
        if (input.isEmpty())
            throw new NullPointerException();
        
        String[] tmp = input.split(sep);
        
        
        if (tmp.length == 1) {
            return new SimpleEntry<Integer, String>(0, input);
        }

        Entry<Integer, String> result = new Entry<Integer, String>() {
            @Override
            public Integer getKey() {
                int id = Integer.parseInt(tmp[0]);

                return id;
            }

            @Override
            public String getValue() {
                return tmp[1];
            }

            @Override
            public String setValue(String value) {
                throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
            }
        };
        
        return result;
    }
}
