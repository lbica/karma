/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.irealsoftware.karma.helpers;

import com.irealsoftware.karma.entities.DataItemBase;
import com.irealsoftware.karma.entities.UINodeInternalData;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.component.UIComponent;
import javax.faces.context.ResponseWriter;

/**
 *
 * @author Laurentiu
 */
public final class HtmlHelper {

    private static final String NOTSELECTEDVALUE = "No Selection";

    public static void createHtmlSelectOptionsValue(ResponseWriter responseWriter, UIComponent component, 
            UINodeInternalData internalData, String excludeValue, boolean includeNotSelectionOpt, boolean required) throws IOException {

        List<DataItemBase> listData = internalData.getListData();

        Object selectedValue = internalData.getSelectedValue();

        if (includeNotSelectionOpt) {
            responseWriter.startElement("option", component);
            responseWriter.writeAttribute("value", "none", null);
            
            responseWriter.writeText("-Select-", null);

            responseWriter.endElement("option");
        }

        listData.forEach((record) -> {
            String value = record.getExpressionValue();
            String text = record.getGuiName();

            //exclude a filtered value
            if (text.equals(excludeValue)) {
                return;
            }

            try {

                responseWriter.startElement("option", component);
                responseWriter.writeAttribute("value", value, null);

                if (record.getGuiName().equals(selectedValue)) {
                    responseWriter.writeAttribute("selected", "selected", null);
                }

                responseWriter.writeText(text, null);

                responseWriter.endElement("option");
            } catch (IOException ex) {
                Logger.getLogger(HtmlHelper.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

    }

    public static void createHtmlInputTypeTextValue(ResponseWriter responseWriter, UIComponent component, UINodeInternalData internalData) {

        Object selectedValue = internalData.getSelectedValue();

        try {
            responseWriter.writeAttribute("value", selectedValue, null);

        } catch (IOException ex) {
            Logger.getLogger(HtmlHelper.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
