package com.irealsoftware.karma.facade;

import com.irealsoftware.karma.entities.DataItemBase;
import com.irealsoftware.karma.entities.DataItemToken;
import com.irealsoftware.karma.entities.Nomenvironments;
import com.irealsoftware.karma.entities.Project;
import com.irealsoftware.karma.entities.Strategy;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.ParameterMode;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.StoredProcedureQuery;

/**
 *
 * @author juneau
 */
@Stateless
public class DAOFacade extends AbstractFacade<Nomenvironments> {

    @PersistenceContext(unitName = "karmaPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public DAOFacade() {
        super(Nomenvironments.class);
    }

    public List<Nomenvironments> getNomenvironmentsList() {

        List<Nomenvironments> authorBookList = em.createNamedQuery("Nomenvironments.findAll").getResultList();

        return authorBookList;
    }

    /*
    *@return Production instance
     */
    public Nomenvironments getProdNomenvironments() {

        Nomenvironments result = (Nomenvironments) em.createNamedQuery("Nomenvironments.findByIsproduction")
                .setParameter("isproduction", 1)
                .getSingleResult();

        return result;
    }

    /*
    *@return Sandbox instance
     */
    public Nomenvironments getSandboxNomenvironments() {

        Query qry = em.createNamedQuery("Nomenvironments.findByIsproduction")
                .setParameter("isproduction", 0);

        Nomenvironments result = (Nomenvironments) qry.getSingleResult();

        return result;
    }

    /*
    *@return list of Project
     */
    public List getProjectsByEnv(int environmentId) {

        Query qry = em.createNativeQuery("SELECT  G.ID as id,  G.ENVIRONMENTID as ENVIRONMENTID,  "
                + "G.GUINAME || CASE G.GROUPTYPE WHEN 'A' THEN ' (Async)' ELSE ' (Sync)' END as GUINAME "
                + "FROM TBLGROUPS G "
                + "JOIN NOMNODEFORMATS FD ON FD.OPERATIONALNAME = 'GROUP' AND FD.OPERATIONALCONTEXT = 'DEFAULT' "
                + "LEFT JOIN NOMNODEFORMATS FI ON FI.OPERATIONALNAME = 'GROUP' AND FI.OPERATIONALCONTEXT = G.OPERATIONALSTATUS "
                + "WHERE G.ENVIRONMENTID = TO_NUMBER(?1)"
                + "AND ( "
                + "(INSTR(?2, G.OPERATIONALSTATUS) > 0) "
                + "OR "
                + "(G.OPERATIONALSTATUS IN ('D')  AND ?3 > 0) "
                + ")", com.irealsoftware.karma.entities.Project.class)
                .setParameter("1", environmentId)
                .setParameter("2", "A,S,H%A,S,H")
                .setParameter("3", 1);

        List result = qry.getResultList();

        return result;
    }

    /*
    *@return Project instance
     */
    public Project findFirstProjectByEnv(int environmentId) {

        Query qry = em.createNativeQuery("SELECT  G.ID as ID,  G.ENVIRONMENTID as ENVIRONMENTID,  "
                + "G.GUINAME || CASE G.GROUPTYPE WHEN 'A' THEN ' (Async)' ELSE ' (Sync)' END GUINAME "
                + "FROM TBLGROUPS G "
                + "JOIN NOMNODEFORMATS FD ON FD.OPERATIONALNAME = 'GROUP' AND FD.OPERATIONALCONTEXT = 'DEFAULT' "
                + "LEFT JOIN NOMNODEFORMATS FI ON FI.OPERATIONALNAME = 'GROUP' AND FI.OPERATIONALCONTEXT = G.OPERATIONALSTATUS "
                + "WHERE G.ENVIRONMENTID = TO_NUMBER(?1)"
                + "AND ( "
                + "(INSTR(?2, G.OPERATIONALSTATUS) > 0) "
                + "OR "
                + "(G.OPERATIONALSTATUS IN ('D')  AND ?3 > 0) "
                + ")", com.irealsoftware.karma.entities.Project.class)
                .setParameter("1", environmentId)
                .setParameter("2", "A,S,H%A,S,H")
                .setParameter("3", 1);

        Query newQry = qry.setMaxResults(1);

        Project result = (Project) newQry.getSingleResult();

        return result;
    }

    /*
    *@parameter1: group id
    *@return Lts o strategies
     */
    public List getStrategiesByEnvProject(int environmentId, int groupId) {

        Query qry = em.createNativeQuery("SELECT  GI.ID as ID,  G.ID as PROJECTID, "
                + "GI.GUINAME as GUINAME, G.ENVIRONMENTID as ENVIRONMENTID "
                + "FROM TBLGROUPS G "
                + "JOIN TBLGROUPITEMS GI ON GI.GROUPID = G.ID AND GI.ENVIRONMENTID = G.ENVIRONMENTID "
                + "JOIN NOMNODEFORMATS FD ON FD.OPERATIONALNAME = 'GROUPITEM' AND FD.OPERATIONALCONTEXT = 'DEFAULT' "
                + "LEFT JOIN NOMNODEFORMATS FI ON FI.OPERATIONALNAME = 'GROUPITEM' AND FI.OPERATIONALCONTEXT = G.OPERATIONALSTATUS || GI.OPERATIONALSTATUS "
                + "WHERE G.ENVIRONMENTID = TO_NUMBER(?1) "
                + "AND ( "
                + "(INSTR(?2, G.OPERATIONALSTATUS) > 0) "
                + ") "
                + "AND "
                + "( "
                + "(INSTR(?2, GI.OPERATIONALSTATUS) > 0) "
                + "OR "
                + "(GI.OPERATIONALSTATUS IN ('D')  AND ?3 > 0) "
                + ") "
                + "AND ( "
                + "(?3 >  0) "
                + "OR "
                + "(?3 <= 0 AND GI.OPERATIONALSTATUS NOT IN ('D')) "
                + ")  "
                + "AND GROUPID = ?4", com.irealsoftware.karma.entities.Strategy.class)
                .setParameter("1", environmentId)
                .setParameter("2", "A,S,H%A,S,H")
                .setParameter("3", 1)
                .setParameter("4", groupId);

        List result = qry.getResultList();

        return result;
    }

    /*
    *@parameter1: group id
    *@return Lts o strategies
     */
    public List getNodeTypeByEnvProject(int environmentId, int groupId) {

        StoredProcedureQuery qry = em.createStoredProcedureQuery("PACK_APP.NODETYPELIST_N", com.irealsoftware.karma.entities.NodeType.class)
                .registerStoredProcedureParameter(1, Long.class,
                        ParameterMode.IN)
                .registerStoredProcedureParameter(2, Long.class,
                        ParameterMode.IN)
                .registerStoredProcedureParameter(3, Class.class,
                        ParameterMode.REF_CURSOR)
                .setParameter(1, environmentId)
                .setParameter(2, groupId);

        qry.execute();

        List result = qry.getResultList();

        return result;
    }

    public List<DataItemBase> NodeUtilGIEList(int nodeId, int environmentId, String rw) {

        StoredProcedureQuery qry = em.createStoredProcedureQuery("PACK_APP.NODEUTILGIELIST", com.irealsoftware.karma.entities.DataItemBase.class)
                .registerStoredProcedureParameter(1, Long.class,
                        ParameterMode.IN)
                .registerStoredProcedureParameter(2, Long.class,
                        ParameterMode.IN)
                .registerStoredProcedureParameter(3, String.class,
                        ParameterMode.IN)
                .registerStoredProcedureParameter(4, Class.class,
                        ParameterMode.REF_CURSOR)
                .setParameter(1, nodeId)
                .setParameter(2, environmentId)
                .setParameter(3, rw);

        qry.execute();

        List result = qry.getResultList();

        return result;
    }

    public List<DataItemBase> NodeUtilMethodList(int nodeId, int environmentId) {

        StoredProcedureQuery qry = em.createStoredProcedureQuery("PACK_APP.NODEUTILMETHODLIST", com.irealsoftware.karma.entities.DataItemBase.class)
                .registerStoredProcedureParameter(1, Long.class,
                        ParameterMode.IN)
                .registerStoredProcedureParameter(2, Long.class,
                        ParameterMode.IN)
                .registerStoredProcedureParameter(3, Class.class,
                        ParameterMode.REF_CURSOR)
                .setParameter(1, nodeId)
                .setParameter(2, environmentId);

        qry.execute();

        List result = qry.getResultList();

        return result;
    }

    public List<DataItemToken> NodeUtilLoad_N(int nodeId, int environmentId) {

        StoredProcedureQuery qry = em.createStoredProcedureQuery("PACK_APP.NODEUTILLOAD_N", com.irealsoftware.karma.entities.DataItemToken.class)
                .registerStoredProcedureParameter(1, Long.class,
                        ParameterMode.IN)
                .registerStoredProcedureParameter(2, Long.class,
                        ParameterMode.IN)
                .registerStoredProcedureParameter(3, Class.class,
                        ParameterMode.REF_CURSOR)
                .setParameter(1, nodeId)
                .setParameter(2, environmentId);

        qry.execute();

        List result = qry.getResultList();

        return result;
    }

    public int NodeSaveExpression(int p_numNodeId, int p_numEnvironmentId, String p_strExpressionText,
            int p_numSYS_SEQ_ID, String p_strUserApp) {

        StoredProcedureQuery qry = em.createStoredProcedureQuery("PACK_APP.NodeSaveExpression")
                .registerStoredProcedureParameter(1, Long.class,
                        ParameterMode.IN)
                .registerStoredProcedureParameter(2, Long.class,
                        ParameterMode.IN)
                .registerStoredProcedureParameter(3, String.class,
                        ParameterMode.IN)
                .registerStoredProcedureParameter(4, Long.class,
                        ParameterMode.OUT)
                .registerStoredProcedureParameter(5, Long.class,
                        ParameterMode.IN)
                .registerStoredProcedureParameter(6, String.class,
                        ParameterMode.IN)
                .setParameter(1, p_numNodeId)
                .setParameter(2, p_numEnvironmentId)
                .setParameter(3, p_strExpressionText)
                .setParameter(5, 0)
                .setParameter(6, null);

        qry.execute();

        int numGroupItemExpressionId = (Integer) qry.getOutputParameterValue(4);

        return numGroupItemExpressionId;
    }

    public void NodeSaveExpressionToken(int p_numNodeId, int p_numEnvironmentId, int p_numGroupItemExpressionId, String p_strType, String p_strTokenText,
            int p_numTypeId, String p_strGUIControlName, int p_numSYS_SEQ_ID, String p_strUserApp) {

        StoredProcedureQuery qry = em.createStoredProcedureQuery("PACK_APP.NodeSaveExpressionToken")
                .registerStoredProcedureParameter(1, Long.class,
                        ParameterMode.IN)
                .registerStoredProcedureParameter(2, Long.class,
                        ParameterMode.IN)
                .registerStoredProcedureParameter(3, Long.class,
                        ParameterMode.IN)
                .registerStoredProcedureParameter(4, String.class,
                        ParameterMode.IN)
                .registerStoredProcedureParameter(5, String.class,
                        ParameterMode.IN)
                .registerStoredProcedureParameter(6, Long.class,
                        ParameterMode.IN)
                .registerStoredProcedureParameter(7, String.class,
                        ParameterMode.IN)
                .registerStoredProcedureParameter(8, Long.class,
                        ParameterMode.IN)
                .registerStoredProcedureParameter(9, String.class,
                        ParameterMode.IN)
                .setParameter(1, p_numNodeId)
                .setParameter(2, p_numEnvironmentId)
                .setParameter(3, p_numGroupItemExpressionId)
                .setParameter(4, p_strType)
                .setParameter(5, p_strTokenText)
                .setParameter(6, p_numTypeId)
                .setParameter(7, p_strGUIControlName)
                .setParameter(8, p_numSYS_SEQ_ID)
                .setParameter(9, p_strUserApp);

        qry.execute();

    }

    public void NodeUtilDeleteTokens(int p_numGroupItemExpressionId, int p_numEnvironmentId) {

        StoredProcedureQuery qry = em.createStoredProcedureQuery("PACK_APP.NodeUtilDeleteTokens")
                .registerStoredProcedureParameter(1, Long.class,
                        ParameterMode.IN)
                .registerStoredProcedureParameter(2, Long.class,
                        ParameterMode.IN)
                .setParameter(1, p_numGroupItemExpressionId)
                .setParameter(2, p_numEnvironmentId);

        qry.execute();

    }

    /*
    *@parameter1: group id
    *@return Lts o strategies
     */
    public Strategy findFirstStrategyByEnvProject(int environmentId, int groupId) {

        Query qry = em.createNativeQuery("SELECT  GI.ID as ID,  G.ID as PROJECTID, "
                + "GI.GUINAME GUINAME, G.ENVIRONMENTID as ENVIRONMENTID "
                + "FROM TBLGROUPS G "
                + "JOIN TBLGROUPITEMS GI ON GI.GROUPID = G.ID AND GI.ENVIRONMENTID = G.ENVIRONMENTID "
                + "JOIN NOMNODEFORMATS FD ON FD.OPERATIONALNAME = 'GROUPITEM' AND FD.OPERATIONALCONTEXT = 'DEFAULT' "
                + "LEFT JOIN NOMNODEFORMATS FI ON FI.OPERATIONALNAME = 'GROUPITEM' AND FI.OPERATIONALCONTEXT = G.OPERATIONALSTATUS || GI.OPERATIONALSTATUS "
                + "WHERE G.ENVIRONMENTID = TO_NUMBER(?1) "
                + "AND ( "
                + "(INSTR(?2, G.OPERATIONALSTATUS) > 0) "
                + ") "
                + "AND "
                + "( "
                + "(INSTR(?2, GI.OPERATIONALSTATUS) > 0) "
                + "OR "
                + "(GI.OPERATIONALSTATUS IN ('D')  AND ?3 > 0) "
                + ") "
                + "AND ( "
                + "(?3 >  0) "
                + "OR "
                + "(?3 <= 0 AND GI.OPERATIONALSTATUS NOT IN ('D')) "
                + ")  "
                + "AND GROUPID = ?4", com.irealsoftware.karma.entities.Strategy.class)
                .setParameter("1", environmentId)
                .setParameter("2", "A,S,H%A,S,H")
                .setParameter("3", 1)
                .setParameter("4", groupId);

        Query newQry = qry.setMaxResults(1);

        Strategy result = (Strategy) newQry.getSingleResult();

        return result;
    }

    /*
    *@parameter1: group id
    *@return Lts o strategies
     */
    public List loadDetailsTree(int envId, int strategyId) {

        Query qry = em.createNativeQuery("SELECT  A.ID, A.TYPE, A.PARENTID, A.PARENTTYPE, A.OPERATIONALSTATUS, A.SYS_SEQ_ID, A.PRIORITYID,\n"
                + "                CASE\n"
                + "                    WHEN A.NODEOPNAME = 'FILTER_OTHER' THEN 'All Others'\n"
                + "                    WHEN A.OPERATIONALSTATUS = 'N' THEN '{undefined}'\n"
                + "                    ELSE NVL(A.GUITEXT, '{undefined}')\n"
                + "                END GUITEXT,\n"
                + "                A.GROUPID PROJECTID, A.GROUPITEMID STRATEGYID, ENVIRONMENTID\n"
                + "        FROM\n"
                + "        (\n"
                + "            SELECT  GI.ID, 'GROUPITEM' TYPE, NULL PARENTID, NULL PARENTTYPE, GI.OPERATIONALSTATUS, -GI.SYS_SEQ_ID SYS_SEQ_ID, 0 PRIORITYID,\n"
                + "                    GI.GUINAME GUITEXT,\n"
                + "                    GI.GROUPID, GI.ID GROUPITEMID,\n"
                + "                    NULL NODEOPNAME, GI.ENVIRONMENTID\n"
                + "--                    FD.BGNDCOLOR BGNDCOLOR, FD.FORECOLOR FORECOLOR,\n"
                + "--                     1 ISBOLD, FD.ICON ICON,\n"
                + "--                    FD.SELECTEDICON SELECTEDICON, FD.EXPANDEDICON EXPANDEDICON\n"
                + "                FROM TBLGROUPS G\n"
                + "                JOIN TBLGROUPITEMS GI ON GI.GROUPID = G.ID AND GI.ENVIRONMENTID = G.ENVIRONMENTID\n"
                + "                --JOIN NOMNODEFORMATS FD ON FD.OPERATIONALNAME = 'GROUPITEM' AND NVL(FD.OPERATIONALCONTEXT,'DEFAULT') = 'DEFAULT' OR FD.OPERATIONALCONTEXT = G.OPERATIONALSTATUS || GI.OPERATIONALSTATUS \n"
                + "                WHERE G.ENVIRONMENTID = ?1\n"
                + "                  AND GI.ID = ?2\n"
                + "            UNION ALL\n"
                + "            SELECT  N.ID, NT.OPERATIONALNAME TYPE, NVL(NP.ID, N.GROUPITEMID) PARENTID, NVL(NTP.OPERATIONALNAME, 'GROUPITEM') PARENTTYPE,\n"
                + "                    N.OPERATIONALSTATUS, N.SYS_SEQ_ID, N.SHOWORDER PRIORITYID,\n"
                + "                    E.EXPRESSIONTEXT GUITEXT,\n"
                + "                    GI.GROUPID, GI.ID GROUPITEMID,\n"
                + "                    NT.OPERATIONALNAME NODEOPNAME, GI.ENVIRONMENTID\n"
                + "--                    FD.BGNDCOLOR BGNDCOLOR, FD.FORECOLOR FORECOLOR,\n"
                + "--                    FD.ISBOLD ISBOLD, FD.ICON ICON,\n"
                + "--                    FD.SELECTEDICON SELECTEDICON, FD.EXPANDEDICON EXPANDEDICON\n"
                + "                FROM TBLGROUPITEMS GI\n"
                + "                JOIN TBLGROUPITEMNODES N ON N.GROUPITEMID = GI.ID AND N.ENVIRONMENTID = GI.ENVIRONMENTID\n"
                + "                LEFT JOIN TBLGROUPITEMEXPRESSIONS E ON E.ENVIRONMENTID = N.ENVIRONMENTID AND E.GROUPITEMNODEID = N.ID\n"
                + "                JOIN NOMNODETYPES NT ON NT.ID = N.NODETYPEID\n"
                + "                LEFT JOIN TBLGROUPITEMNODES NP\n"
                + "                  ON NP.ENVIRONMENTID = N.ENVIRONMENTID AND NP.ID = N.PARENTID\n"
                + "                 AND NP.GROUPITEMID = N.GROUPITEMID\n"
                + "                LEFT JOIN NOMNODETYPES NTP ON NTP.ID = NP.NODETYPEID\n"
                + "                --JOIN NOMNODEFORMATS FD ON FD.OPERATIONALNAME = NT.OPERATIONALNAME AND (FD.OPERATIONALCONTEXT = 'DEFAULT' OR NVL(FD.OPERATIONALCONTEXT,'') = '') \n"
                + "                WHERE GI.ENVIRONMENTID = ?1\n"
                + "                  AND GI.ID = ?2 AND n.OPERATIONALSTATUS NOT IN ('D')\n"
                + "            ) A\n"
                + "            WHERE (A.SYS_SEQ_ID > 0 OR A.TYPE = 'GROUPITEM') ORDER BY A.PRIORITYID, A.GUITEXT",
                com.irealsoftware.karma.entities.StrategyNode.class)
                .setParameter("1", envId)
                .setParameter("2", strategyId);

        List result = qry.getResultList();

        return result;
    }

    public List getStrategyNodes(int envId, int projectId, int strategyId) {

        Query qry = em.createNativeQuery("SELECT ID, ENVIRONMENTID, PROJECTID, STRATEGYID, GUINAME FROM (SELECT gin.ID , gi.ENVIRONMENTID as ENVIRONMENTID, gi.GROUPID as PROJECTID, NVL(gin.PARENTID, 999999) as STRATEGYID, "
                + "CASE WHEN nt.OPERATIONALNAME = 'FILTER_OTHER' THEN 'All Others' ELSE E.EXPRESSIONTEXT END as GUINAME, gin.SHOWORDER as PRIORITYID "
                + "FROM TBLGROUPS g "
                + "INNER JOIN TBLGROUPITEMS gi  "
                + "ON gi.GROUPID = g.ID  AND gi.ENVIRONMENTID = g.ENVIRONMENTID "
                + "INNER JOIN TBLGROUPITEMNODES gin "
                + "ON gi.ID = gin.GROUPITEMID and gi.ENVIRONMENTID = gin.ENVIRONMENTID "
                + "LEFT JOIN TBLGROUPITEMEXPRESSIONS E ON E.GROUPITEMNODEID = gin.ID and E.ENVIRONMENTID = gin.ENVIRONMENTID "
                + "INNER JOIN NOMNODETYPES NT ON NT.ID = gin.NODETYPEID "
                + "WHERE G.ENVIRONMENTID = ?1 AND G.ID = ?2 AND gi.ID = ?3 AND gin.OPERATIONALSTATUS NOT IN ('D')"
                + "UNION ALL "
                + "SELECT 999999 as ID, gi.ENVIRONMENTID as ENVIRONMENTID, gi.GROUPID as PROJECTID, NULL as STRATEGYID, gi.GUINAME as GUINAME, 0 PRIORITYID "
                + "FROM TBLGROUPITEMS gi where ENVIRONMENTID = ?1 AND GROUPID = ?2 AND ID = ?3) WHERE GUINAME IS NOT NULL ORDER BY PRIORITYID", com.irealsoftware.karma.entities.StrategyNode.class)
                .setParameter("1", envId)
                .setParameter("2", projectId)
                .setParameter("3", strategyId);

        List result = qry.getResultList();

        return result;
    }

    public void pasteOneNode(Integer numcopyfromid, Integer numpastetoid, Integer strategyId, String strMode, Integer environmentId, String userApp) {

        StoredProcedureQuery spqry = em.createStoredProcedureQuery("PACK_APP.PASTEONENODE");

        spqry.registerStoredProcedureParameter(1, Integer.class, ParameterMode.IN)
                .setParameter(1, numcopyfromid);

        spqry.registerStoredProcedureParameter(2, Integer.class, ParameterMode.IN)
                .setParameter(2, numpastetoid);

        spqry.registerStoredProcedureParameter(3, Integer.class, ParameterMode.IN)
                .setParameter(3, strategyId);

        spqry.registerStoredProcedureParameter(4, String.class, ParameterMode.IN)
                .setParameter(4, strMode);

        spqry.registerStoredProcedureParameter(5, Integer.class, ParameterMode.IN)
                .setParameter(5, environmentId);

        spqry.registerStoredProcedureParameter(6, String.class, ParameterMode.IN)
                .setParameter(6, userApp);

        spqry.execute();
    }

    public void nodeSave(Integer numnodeid, Integer numenvironmentid, Integer numgroupitemid, Integer numrelatednodeid,
            Integer numrelatedtypeid, Integer numrelatedsys_seq_id, Integer numshoworder, Integer numnodetypeid,
            String strnodecode, String strnodegroup, Integer numnodetopiterator, String stroperationalstatus, Integer numsys_seq_id, String struserapp) {

        StoredProcedureQuery spqry = em.createStoredProcedureQuery("PACK_APP.NODESAVE");

        spqry.registerStoredProcedureParameter(1, Integer.class, ParameterMode.INOUT)
                .setParameter(1, numnodeid);

        spqry.registerStoredProcedureParameter(2, Integer.class, ParameterMode.IN)
                .setParameter(2, numenvironmentid);

        spqry.registerStoredProcedureParameter(3, Integer.class, ParameterMode.IN)
                .setParameter(3, numgroupitemid);

        spqry.registerStoredProcedureParameter(4, Integer.class, ParameterMode.IN)
                .setParameter(4, numrelatednodeid);

        spqry.registerStoredProcedureParameter(5, Integer.class, ParameterMode.IN)
                .setParameter(5, numrelatedtypeid);

        spqry.registerStoredProcedureParameter(6, Integer.class, ParameterMode.IN)
                .setParameter(6, numrelatedsys_seq_id);

        spqry.registerStoredProcedureParameter(7, Integer.class, ParameterMode.IN)
                .setParameter(7, numshoworder);

        spqry.registerStoredProcedureParameter(8, Integer.class, ParameterMode.IN)
                .setParameter(8, numnodetypeid);

        spqry.registerStoredProcedureParameter(9, String.class, ParameterMode.IN)
                .setParameter(9, strnodecode);

        spqry.registerStoredProcedureParameter(10, String.class, ParameterMode.IN)
                .setParameter(10, strnodegroup);

        spqry.registerStoredProcedureParameter(11, Integer.class, ParameterMode.IN)
                .setParameter(11, numnodetopiterator);

        spqry.registerStoredProcedureParameter(12, String.class, ParameterMode.IN)
                .setParameter(12, stroperationalstatus);

        spqry.registerStoredProcedureParameter(13, Integer.class, ParameterMode.IN)
                .setParameter(13, numsys_seq_id);

        spqry.registerStoredProcedureParameter(14, String.class, ParameterMode.IN)
                .setParameter(14, struserapp);

        spqry.execute();
    }

    /**
     * Recipe 10-1
     *
     * @return
     */
//    public List<Nomenvironments> findAuthor() {
//        return em.createQuery("select o from BookAuthor o")
//                .getResultList();
//    }
//
//    /**
//     * Recipe 10-10
//     *
//     */
//    public List<Nomenvironments> findAuthorByLast(String authorLast) {
//        return em.createQuery("select o from BookAuthor o "
//                + "where o.last = UPPER(:authorLast)")
//                .setParameter("authorLast", authorLast).getResultList();
//    }
//
//    /**
//     * Return BookAuthor object given a specified authorId
//     *
//     * @param authorId
//     * @return
//     */
//    public Nomenvironments findByAuthorId(BigDecimal authorId) {
//        return (Nomenvironments) em.createQuery("select object(o) from BookAuthor o "
//                + "where o.id = :id")
//                .setParameter("id", authorId)
//                .getSingleResult();
//    }
//
//    /**
//     * Recipe 10-5 Using SqlResultSetMapping
//     *
//     * @return
//     */
//    public List findAuthorBooksMapping() {
//
//        Query qry = em.createNativeQuery(
//                "select b.id as BOOK_ID, b.title as TITLE, "
//                + "ba.id AS AUTHOR_ID, ba.firstname, ba.lastname "
//                + "from book_author ba, book b, author_work aw "
//                + "where aw.author_id = ba.id "
//                + "and b.id = aw.book_id", "authorBooks");
//
//        return qry.getResultList();
//    }
//
//    /**
//     * 10-5 Without SqlResultSetMapping
//     *
//     * @return
//     */
//    public List<Map> findAuthorBooks() {
//
//        Query qry = em.createNativeQuery(
//                "select ba.id, ba.lastname, ba.firstname, ba.bio, b.id, b.title, b.image, b.description "
//                + "from book_author ba, book b, author_work aw "
//                + "where aw.author_id = ba.id "
//                + "and b.id = aw.book_id");
//
//        List<Object[]> results = qry.getResultList();
//        List data = new ArrayList<HashMap>();
//
//        if (!results.isEmpty()) {
//            for (Object[] result : results) {
//                HashMap resultMap = new HashMap();
//                resultMap.put("authorId", result[0]);
//                resultMap.put("authorLast", result[1]);
//                resultMap.put("authorFirst", result[2]);
//                resultMap.put("authorBio", result[3]);
//                resultMap.put("bookId", result[4]);
//                resultMap.put("bookTitle", result[5]);
//                resultMap.put("bookImage", result[6]);
//                resultMap.put("bookDescription", result[7]);
//
//                data.add(resultMap);
//            }
//
//        }
//        return data;
//    }
    /**
     * public void createUser(String username, String password){
     * StoredProcedureQuery qry = em.createStoredProcedureQuery("createUser")
     * .setParameter("USER", username) .setParameter("PASS",password);
     *
     * try { qry.executeUpdate(); } catch (Exception ex) {
     * System.out.println(ex); } }
     *
     */
    /**
     * @return the authorBookList
     */
}
