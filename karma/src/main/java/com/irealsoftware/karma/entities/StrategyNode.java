/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.irealsoftware.karma.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Laurentiu
 */

@Entity
public class StrategyNode implements Serializable {

    public StrategyNode() {
    }


    @Id
    private Integer id;
    
    private String type;
    
    private Integer parentId;
    
    private String operationalStatus;
    
    private Integer priorityId;

    public Integer getPriorityId() {
        return priorityId;
    }

    public void setPriorityId(Integer priorityId) {
        this.priorityId = priorityId;
    }

    public String getOperationalStatus() {
        return operationalStatus;
    }

    public void setOperationalStatus(String operationalStatus) {
        this.operationalStatus = operationalStatus;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
//    @EmbeddedId
//    private NodeHierarchyPK id;

//    public NodeHierarchy(Integer id, Integer strategyId) {
//        this.id = new NodeHierarchyPK(id, strategyId);
//    }
//
//    public NodeHierarchyPK getId() {
//        return id;
//    }
//
//    public void setId(NodeHierarchyPK id) {
//        this.id = id;
//    }
    
    private Integer environmentId;

    public Integer getEnvironmentId() {
        return environmentId;
    }

    public void setEnvironmentId(Integer environmentId) {
        this.environmentId = environmentId;
    }
    
    private String guiText;
    
    private Integer strategyId;
    
    private Integer projectId;

    public Integer getStrategyId() {
        return strategyId;
    }

    public void setStrategyId(Integer strategyId) {
        this.strategyId = strategyId;
    }

    public String getGuiText() {
        return guiText;
    }

    public void setGuiText(String guiText) {
        this.guiText = guiText;
    }

    

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }
    
}
