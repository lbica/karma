/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.irealsoftware.karma.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Laurentiu
 */
@Entity
public class DataItemTop {

    @Id
    private Integer groupItemNodeId;

    public Integer getGroupItemNodeId() {
        return groupItemNodeId;
    }

    public void setGroupItemNodeId(Integer groupItemNodeId) {
        this.groupItemNodeId = groupItemNodeId;
    }

    public Integer getTopCount() {
        return topCount;
    }

    public void setTopCount(Integer topCount) {
        this.topCount = topCount;
    }

    public String getTopType() {
        return topType;
    }

    public void setTopType(String topType) {
        this.topType = topType;
    }

    public String getTopTiesType() {
        return topTiesType;
    }

    public void setTopTiesType(String topTiesType) {
        this.topTiesType = topTiesType;
    }

    public String getDataOrder() {
        return dataOrder;
    }

    public void setDataOrder(String dataOrder) {
        this.dataOrder = dataOrder;
    }

    public String getNullOrder() {
        return nullOrder;
    }

    public void setNullOrder(String nullOrder) {
        this.nullOrder = nullOrder;
    }

    public String getNullInclude() {
        return nullInclude;
    }

    public void setNullInclude(String nullInclude) {
        this.nullInclude = nullInclude;
    }

    public Integer getEnvironmentId() {
        return environmentId;
    }

    public void setEnvironmentId(Integer environmentId) {
        this.environmentId = environmentId;
    }

    private Integer topCount;

    private String topType;

    private String topTiesType;

    private String dataOrder;

    private String nullOrder;

    private String nullInclude;

    private Integer environmentId;

}
