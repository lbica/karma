/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.irealsoftware.karma.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Laurentiu
 */
@Entity
public class NodeType implements Serializable {

    @Id
    private Integer id;

    private String guiName;
    
    private String operationalName;

    public String getOperationalName() {
        return operationalName;
    }

    public void setOperationalName(String operationalName) {
        this.operationalName = operationalName;
    }

    public String getGuiName() {
        return guiName + "(" + id + ")";
    }

    public void setGuiName(String guiName) {
        this.guiName = guiName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
