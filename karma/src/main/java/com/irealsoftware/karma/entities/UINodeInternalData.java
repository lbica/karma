/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.irealsoftware.karma.entities;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Laurentiu
 */
public class UINodeInternalData implements Serializable {

    private Integer id;

    private List<DataItemBase> listData;

    private Object selectedValue;

    //Add(TokenType As String, TextValue As Variant, GUIControl As Variant, RefId As Variant, Optional sKey As String) As CToken
    private DataItemToken itemToken;

    public DataItemToken getItemToken() {
        return itemToken;
    }

    public void setItemToken(DataItemToken itemToken) {
         this.itemToken= itemToken;

    }

    public void copyItemToken(DataItemToken itemToken) {
        
            if (itemToken.getTokenType() != TokenTypeEnum.VALUE) {
            this.itemToken.setTokenRefId(itemToken.getTokenRefId());
        }
        
        this.itemToken.setGuiControlName(itemToken.getGuiControlName());
        this.itemToken.setTokenSetName(itemToken.getTokenSetName());
        this.itemToken.setTokenText(itemToken.getTokenText());
        this.itemToken.setTokenType(itemToken.getTokenType());

    }

    public UINodeInternalData() {

    }

    public UINodeInternalData(List<DataItemBase> list) {
        this();
        this.listData = list;
    }

    public UINodeInternalData(List<DataItemBase> list, DataItemToken itemToken) {
        this();
        this.listData = list;
        this.itemToken = itemToken;
    }

    public Object getSelectedValue() {
        return selectedValue;
    }

    public void setSelectedValue(Object selectedValue) {
        this.selectedValue = selectedValue;
    }

    public List<DataItemBase> getListData() {
        return listData;
    }

    public void setListData(List<DataItemBase> listData) {
        this.listData = listData;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
