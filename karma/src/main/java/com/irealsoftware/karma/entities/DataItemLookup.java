/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.irealsoftware.karma.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Laurentiu
 */
@Entity
public class DataItemLookup {

    @Id
    private Integer groupItemElementId;

    private String groupItemValue;

    private Integer dataTableId;

    private Integer dataTableFieldId;

    private String role;

    public Integer getGroupItemElementId() {
        return groupItemElementId;
    }

    public void setGroupItemElementId(Integer groupItemElementId) {
        this.groupItemElementId = groupItemElementId;
    }

    public String getGroupItemValue() {
        return groupItemValue;
    }

    public void setGroupItemValue(String groupItemValue) {
        this.groupItemValue = groupItemValue;
    }

    public Integer getDataTableId() {
        return dataTableId;
    }

    public void setDataTableId(Integer dataTableId) {
        this.dataTableId = dataTableId;
    }

    public Integer getDataTableFieldId() {
        return dataTableFieldId;
    }

    public void setDataTableFieldId(Integer dataTableFieldId) {
        this.dataTableFieldId = dataTableFieldId;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

}
