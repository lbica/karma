/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.irealsoftware.karma.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FieldResult;
import javax.persistence.Id;
import javax.persistence.SqlResultSetMapping;

/**
 *
 * @author Laurentiu
 */
@Entity
@SqlResultSetMapping(name="hierarchicallylist",
        entities= {
            @javax.persistence.EntityResult(entityClass=com.irealsoftware.karma.entities.GenericHierarchicallyEntityResult.class, fields={
                @FieldResult(name="id", column="ID"),
                @FieldResult(name="parentid", column="PARENTID"),
                @FieldResult(name="guiname", column="GUITEXT")
            })
        })
public class GenericHierarchicallyEntityResult implements Serializable {
    
    @Id
    private Integer id;
    
    private Integer parentId;
    
    private String guiname;
    
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getGuiname() {
        return guiname;
    }

    public void setGuiname(String guiname) {
        this.guiname = guiname;
    }




    

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }
    
}
