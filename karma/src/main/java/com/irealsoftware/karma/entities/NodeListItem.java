/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.irealsoftware.karma.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Laurentiu
 */
@Entity
public class NodeListItem implements Serializable {

    @Id
    private Integer id;

    private String guiName;
    
    private String guiDescription;

    public String getGuiDescription() {
        return guiDescription;
    }

    public void setGuiDescription(String guiDescription) {
        this.guiDescription = guiDescription;
    }


    public String getGuiName() {
        return guiName;
    }

    public void setGuiName(String guiName) {
        this.guiName = guiName;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
