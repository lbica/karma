/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.irealsoftware.karma.entities;

/**
 *
 * @author Laurentiu
 */
public enum OperationType {
    Add,
    Edit,
    Delete,
    None
}
