/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.irealsoftware.karma.entities;

import org.richfaces.model.SwingTreeNodeImpl;

/**
 *
 * @author Laurentiu
 */
public class NodeHierarchy extends SwingTreeNodeImpl<StrategyNode> {

    private Integer id;
    
    private String guiText;
    
    private NodeHierarchyType nodeType;
    

    public NodeHierarchyType getNodeType() {
        return nodeType;
    }

    public void setNodeType(NodeHierarchyType nodeType) {
        this.nodeType = nodeType;
    }

    public Integer getId() {
        return id;
    }

    

    public String getGuiText() {
        return guiText;
    }

    public NodeHierarchy(String guiText) {
        super();
        this.guiText = guiText;
    }

    public NodeHierarchy(StrategyNode data, NodeHierarchyType nodeType) {
        this(data.getId(), data.getGuiText(), nodeType);
        this.setData(data);

    }

    public NodeHierarchy(Integer id, String guiText, NodeHierarchyType nodeType) {
        super();

        this.id = id;
        this.guiText = guiText;
        
        this.nodeType = nodeType;
    }


    public boolean hasChilds() {
        return this.children().hasMoreElements();
    }

}
