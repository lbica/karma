/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.irealsoftware.karma.entities;

import com.google.common.collect.Lists;
import java.util.List;

/**
 *
 * @author Laurentiu
 */
public class GenericNode {

    public GenericNode(Integer id, String name, GenericNode parent) {
        this.id = id;
        this.name = name;
        this.parent = parent;
    }
    
    private Integer id;
    
    private String name;
    
    private GenericNode parent;
    
    List<GenericNode> childs;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GenericNode getParent() {
        return parent;
    }

    public void setParent(GenericNode parent) {
        this.parent = parent;
    }

    public List<GenericNode> getChilds() {
        
        if (this.childs == null)
            this.childs = Lists.newArrayList();
        
        return this.childs;
    }

    public void setChilds(List<GenericNode> childs) {
        this.childs = childs;
    }
    
}
