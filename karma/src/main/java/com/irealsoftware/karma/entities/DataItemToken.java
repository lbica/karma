/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.irealsoftware.karma.entities;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

/**
 *
 * @author Laurentiu
 */

@Entity
public class DataItemToken implements Serializable {
    
    
    @Id
    private Integer tokenRefId;

    private String tokenSetName;
    
    private String tokenText;
    
    private Boolean isEligibleForSave = true;
    
    private TokenTypeEnum tokenType;

    public TokenTypeEnum getTokenType() {
        return tokenType;
    }

    public void setTokenType(TokenTypeEnum tokenType) {
        this.tokenType = tokenType;
    }

    public Boolean getIsEligibleForSave() {
        return isEligibleForSave;
    }

    public void setIsEligibleForSave(Boolean isEligibleForSave) {
        this.isEligibleForSave = isEligibleForSave;
    }
    
    @Id
    private String guiControlName;

    public Integer getTokenRefId() {
        return tokenRefId;
    }

    public void setTokenRefId(Integer tokenRefId) {
        this.tokenRefId = tokenRefId;
    }

    public String getTokenSetName() {
        return tokenSetName;
    }

    public void setTokenSetName(String tokenSetName) {
        this.tokenSetName = tokenSetName;
    }

    public String getTokenText() {
        return tokenText;
    }

    public void setTokenText(String tokenText) {
        this.tokenText = tokenText;
    }

    public String getGuiControlName() {
        return guiControlName;
    }

    public void setGuiControlName(String guiControlName) {
        this.guiControlName = guiControlName;
    }



}
