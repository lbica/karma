/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.irealsoftware.karma.entities;

import java.io.Serializable;

/**
 *
 * @author Laurentiu
 */


public enum NodeHierarchyType {
        Root,
        Leaf,
        Parent,
        NotDefined,
    }
