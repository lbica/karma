/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.irealsoftware.karma.entities;

import java.util.List;

/**
 *
 * @author Laurentiu
 */
public class EntityResultHierarchy {

    public EntityResultHierarchy(int id, String name, EntityResultHierarchy parent) {
        this.id = id;
        this.name = name;
        this.parent = parent;
    }
    
    private int id;
    
    private String name;
    
    private EntityResultHierarchy parent;

    public EntityResultHierarchy getParent() {
        return parent;
    }

    public void setParent(EntityResultHierarchy parent) {
        this.parent = parent;
    }
    
    private List<EntityResultHierarchy> childs;

    public List<EntityResultHierarchy> getChilds() {
        return childs;
    }

    public void setChilds(List<EntityResultHierarchy> childs) {
        this.childs = childs;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
}
