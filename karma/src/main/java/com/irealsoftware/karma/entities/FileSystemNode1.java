/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.irealsoftware.karma.entities;

/**
 *
 * @author Laurentiu
 */
public class FileSystemNode1 extends org.richfaces.model.SwingTreeNodeImpl<Object> {
   private String label;

    public String getLabel() {
        return label;
    }

   public FileSystemNode1(String label) {
      super();
      this.label = label;
   }

   public String getNodeType() {
      return isLeaf() ? "leaf" : "node";
   }
}
