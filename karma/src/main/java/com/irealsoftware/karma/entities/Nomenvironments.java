/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.irealsoftware.karma.entities;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Laurentiu
 */
@Entity
@Table(name = "NOMENVIRONMENTS")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Nomenvironments.findAll", query = "SELECT n FROM Nomenvironments n")
    , @NamedQuery(name = "Nomenvironments.findById", query = "SELECT n FROM Nomenvironments n WHERE n.id = :id")
    , @NamedQuery(name = "Nomenvironments.findByGuiname", query = "SELECT n FROM Nomenvironments n WHERE n.guiname = :guiname")
    , @NamedQuery(name = "Nomenvironments.findByGuidescription", query = "SELECT n FROM Nomenvironments n WHERE n.guidescription = :guidescription")
    , @NamedQuery(name = "Nomenvironments.findByOperationalschema", query = "SELECT n FROM Nomenvironments n WHERE n.operationalschema = :operationalschema")
    , @NamedQuery(name = "Nomenvironments.findByIsproduction", query = "SELECT n FROM Nomenvironments n WHERE n.isproduction = :isproduction")
    , @NamedQuery(name = "Nomenvironments.findByEnvironmentgroupid", query = "SELECT n FROM Nomenvironments n WHERE n.environmentgroupid = :environmentgroupid")
    , @NamedQuery(name = "Nomenvironments.findBySysSeqId", query = "SELECT n FROM Nomenvironments n WHERE n.sysSeqId = :sysSeqId")
    , @NamedQuery(name = "Nomenvironments.findBySysUserDb", query = "SELECT n FROM Nomenvironments n WHERE n.sysUserDb = :sysUserDb")
    , @NamedQuery(name = "Nomenvironments.findBySysUserOs", query = "SELECT n FROM Nomenvironments n WHERE n.sysUserOs = :sysUserOs")
    , @NamedQuery(name = "Nomenvironments.findBySysUserApp", query = "SELECT n FROM Nomenvironments n WHERE n.sysUserApp = :sysUserApp")
    , @NamedQuery(name = "Nomenvironments.findBySysUserHost", query = "SELECT n FROM Nomenvironments n WHERE n.sysUserHost = :sysUserHost")
    , @NamedQuery(name = "Nomenvironments.findBySysUserProgram", query = "SELECT n FROM Nomenvironments n WHERE n.sysUserProgram = :sysUserProgram")
    , @NamedQuery(name = "Nomenvironments.findBySysCreationDate", query = "SELECT n FROM Nomenvironments n WHERE n.sysCreationDate = :sysCreationDate")
    , @NamedQuery(name = "Nomenvironments.findBySysUpdateDate", query = "SELECT n FROM Nomenvironments n WHERE n.sysUpdateDate = :sysUpdateDate")
    , @NamedQuery(name = "Nomenvironments.findByOperationalstatus", query = "SELECT n FROM Nomenvironments n WHERE n.operationalstatus = :operationalstatus")})
//@SqlResultSetMapping(name="listGroups",
//        entities= {
//            @EntityResult(entityClass=com.irealsoftware.karma.entities.EntityResult.class, fields={
//                @FieldResult(name="id", column="ID"),
//                @FieldResult(name="parentid", column="PARENTID"),
//                @FieldResult(name="guiname", column="GUITEXT")
//            })
//        })
public class Nomenvironments implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "ID")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "GUINAME")
    private String guiname;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 4000)
    @Column(name = "GUIDESCRIPTION")
    private String guidescription;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "OPERATIONALSCHEMA")
    private String operationalschema;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ISPRODUCTION")
    private short isproduction;
    @Basic(optional = false)
    @NotNull
    @Column(name = "ENVIRONMENTGROUPID")
    private BigInteger environmentgroupid;
    @Column(name = "SYS_SEQ_ID")
    private BigInteger sysSeqId;
    @Size(max = 200)
    @Column(name = "SYS_USER_DB")
    private String sysUserDb;
    @Size(max = 200)
    @Column(name = "SYS_USER_OS")
    private String sysUserOs;
    @Size(max = 200)
    @Column(name = "SYS_USER_APP")
    private String sysUserApp;
    @Size(max = 200)
    @Column(name = "SYS_USER_HOST")
    private String sysUserHost;
    @Size(max = 200)
    @Column(name = "SYS_USER_PROGRAM")
    private String sysUserProgram;
    @Column(name = "SYS_CREATION_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date sysCreationDate;
    @Column(name = "SYS_UPDATE_DATE")
    @Temporal(TemporalType.TIMESTAMP)
    private Date sysUpdateDate;
    @Column(name = "OPERATIONALSTATUS")
    private Character operationalstatus;

    public Nomenvironments() {
    }

    public Nomenvironments(Integer id) {
        this.id = id;
    }

    public Nomenvironments(Integer id, String guiname, String guidescription, String operationalschema, short isproduction, BigInteger environmentgroupid) {
        this.id = id;
        this.guiname = guiname;
        this.guidescription = guidescription;
        this.operationalschema = operationalschema;
        this.isproduction = isproduction;
        this.environmentgroupid = environmentgroupid;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGuiname() {
        return guiname + "(" + id + ")";
    }

    public void setGuiname(String guiname) {
        this.guiname = guiname;
    }

    public String getGuidescription() {
        return guidescription;
    }

    public void setGuidescription(String guidescription) {
        this.guidescription = guidescription;
    }

    public String getOperationalschema() {
        return operationalschema;
    }

    public void setOperationalschema(String operationalschema) {
        this.operationalschema = operationalschema;
    }

    public short getIsproduction() {
        return isproduction;
    }

    public void setIsproduction(short isproduction) {
        this.isproduction = isproduction;
    }

    public BigInteger getEnvironmentgroupid() {
        return environmentgroupid;
    }

    public void setEnvironmentgroupid(BigInteger environmentgroupid) {
        this.environmentgroupid = environmentgroupid;
    }

    public BigInteger getSysSeqId() {
        return sysSeqId;
    }

    public void setSysSeqId(BigInteger sysSeqId) {
        this.sysSeqId = sysSeqId;
    }

    public String getSysUserDb() {
        return sysUserDb;
    }

    public void setSysUserDb(String sysUserDb) {
        this.sysUserDb = sysUserDb;
    }

    public String getSysUserOs() {
        return sysUserOs;
    }

    public void setSysUserOs(String sysUserOs) {
        this.sysUserOs = sysUserOs;
    }

    public String getSysUserApp() {
        return sysUserApp;
    }

    public void setSysUserApp(String sysUserApp) {
        this.sysUserApp = sysUserApp;
    }

    public String getSysUserHost() {
        return sysUserHost;
    }

    public void setSysUserHost(String sysUserHost) {
        this.sysUserHost = sysUserHost;
    }

    public String getSysUserProgram() {
        return sysUserProgram;
    }

    public void setSysUserProgram(String sysUserProgram) {
        this.sysUserProgram = sysUserProgram;
    }

    public Date getSysCreationDate() {
        return sysCreationDate;
    }

    public void setSysCreationDate(Date sysCreationDate) {
        this.sysCreationDate = sysCreationDate;
    }

    public Date getSysUpdateDate() {
        return sysUpdateDate;
    }

    public void setSysUpdateDate(Date sysUpdateDate) {
        this.sysUpdateDate = sysUpdateDate;
    }

    public Character getOperationalstatus() {
        return operationalstatus;
    }

    public void setOperationalstatus(Character operationalstatus) {
        this.operationalstatus = operationalstatus;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Nomenvironments)) {
            return false;
        }
        Nomenvironments other = (Nomenvironments) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.irealsoftware.karma.entities.Nomenvironments[ id=" + id + " ]";
    }
    
}
