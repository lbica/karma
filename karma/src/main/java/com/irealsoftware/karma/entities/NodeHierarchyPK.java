/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.irealsoftware.karma.entities;

import java.io.Serializable;
import javax.persistence.Embeddable;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

/**
 *
 * @author Laurentiu
 */

@Embeddable
public class NodeHierarchyPK implements Serializable {

    public NodeHierarchyPK() {
    }

    public NodeHierarchyPK(Integer id, Integer strategyId) {
        this.id = id;
        this.strategyId = strategyId;
    }

    @NotNull
    private Integer id;

    private Integer strategyId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getStrategyId() {
        return strategyId;
    }

    public void setStrategyId(Integer strategyId) {
        this.strategyId = strategyId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (this.id != null ? this.id.hashCode() : 0);
        hash += (this.strategyId != null ? this.strategyId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NodeHierarchyPK)) {
            return false;
        }
        NodeHierarchyPK other = (NodeHierarchyPK) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        if ((this.strategyId == null && other.strategyId != null) || (this.strategyId != null && !this.strategyId.equals(other.strategyId))) {
            return false;
        }
        return true;
    }

}
