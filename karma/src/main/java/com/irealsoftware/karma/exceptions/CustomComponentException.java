/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.irealsoftware.karma.exceptions;

/**
 *
 * @author Laurentiu
 */
public class CustomComponentException extends Exception{

    public CustomComponentException() {
        super("CustomComponentException exception in Karma application");
    }

    public CustomComponentException(String msg) {
        super(msg);
    }

    public CustomComponentException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
