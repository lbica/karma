/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.irealsoftware.karma.exceptions;

/**
 *
 * @author LaurentiuBica
 */
public class NotSupportedException extends Exception {

    public NotSupportedException() {
        super("Not supported exception in Karma application");
    }

    public NotSupportedException(String msg) {
        super(msg);
    }
}
