/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.irealsoftware.karma.exceptions;

/**
 *
 * @author Laurentiu
 */
public class ApplicationException extends Exception {

    public ApplicationException() {
        super("Not supported exception in Karma application");
    }

    public ApplicationException(String msg) {
        super(msg);
    }
    
        public ApplicationException(String msg, Throwable cause) {
        super(msg, cause);
    }
}