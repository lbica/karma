/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.irealsoftware.karma.customcomponents;

import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.irealsoftware.karma.entities.DataItemBase;
import com.irealsoftware.karma.entities.DataItemToken;
import com.irealsoftware.karma.entities.UINodeInternalData;
import com.irealsoftware.karma.entities.NodeTypeEnum;
import com.irealsoftware.karma.entities.StrategyNode;
import com.irealsoftware.karma.entities.TokenTypeEnum;
import com.irealsoftware.karma.exceptions.CustomComponentException;
import com.irealsoftware.karma.exceptions.NotSupportedException;
import com.irealsoftware.karma.facade.DAOFacade;
import com.irealsoftware.karma.helpers.HtmlHelper;
import com.irealsoftware.karma.helpers.StringHelper;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.ResourceDependency;
import javax.faces.component.FacesComponent;
import javax.faces.component.UIComponentBase;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;

/**
 *
 * @author Laurentiu
 */
@FacesComponent("com.irealsoftware.karma.NodeFilter")
@ResourceDependency(library = "js", name = "test_js.js")
public class UINodeFilter extends AbstractUINodeComponent<StrategyNode> {

    private static final String CBOFILTERVAR = "cboFilterVar";

    private static final String CBOOPERATOR1 = "cboOperator1";

    private static final String CBOOPERATOR2 = "cboOperator2";

    private static final String TXTVALUE1 = "txtValue1";

    private static final String TXTVALUE2 = "txtValue2";

    private static final String LBLAND = "lblAND";

    private static final String OPAND = "AND";

    private String[] fieldsLabel = {CBOFILTERVAR, CBOOPERATOR1, CBOOPERATOR2, TXTVALUE1, TXTVALUE2, LBLAND};

    public UINodeFilter(StrategyNode data, DAOFacade dao) {
        super(data, dao);
//        setRendererType(null);
    }

    public void encodeBegin(FacesContext context) throws IOException {
        ResponseWriter responseWriter = context.getResponseWriter();
        String clientId = getClientId(context);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "form-group", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "input-group", null);

        responseWriter.startElement("input", this);
        responseWriter.writeAttribute("class", "form-control input-sm", null);

        responseWriter.writeAttribute("readonly", "readonly", null);

        responseWriter.writeAttribute("value", this.getName(), null);

        responseWriter.endElement("input");
        responseWriter.startElement("span", this);
        responseWriter.writeAttribute("class", "input-group-btn", null);

        responseWriter.startElement("button", this);
        responseWriter.writeAttribute("class", "btn btn-primary btn-sm", null);

        responseWriter.writeAttribute("name", clientId + BTN_SAVENAME, null);

        responseWriter.writeAttribute("type", "submit", null);

        String ajaxScript = MessageFormat.format(
                "{0};", getChangeScript(context, this));

        responseWriter.writeAttribute("onclick", ajaxScript, null);

        responseWriter.writeText("Save", null);

        responseWriter.endElement("button");
        responseWriter.endElement("span");
        responseWriter.endElement("div");
        responseWriter.endElement("div");
        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "form-group", null);

        responseWriter.startElement("label", this);

        responseWriter.writeText("Variable", null);

        responseWriter.endElement("label");
        responseWriter.startElement("select", this);

        //set name for decode in post request
        responseWriter.writeAttribute("name", CBOFILTERVAR, null);

        responseWriter.writeAttribute("class", "form-control input-sm", null);

        responseWriter.writeAttribute("id", "direction", null);

        responseWriter.writeAttribute("name", "variable", null);

        responseWriter.writeAttribute("size", "1", null);

        //create Options tag for cboFilterVar
        HtmlHelper.createHtmlSelectOptionsValue(responseWriter, this, this.internalStorage.get(CBOFILTERVAR), null, true, true);

        responseWriter.endElement("select");
        responseWriter.endElement("div");

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "row", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "col-md-6", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "form-group", null);

        responseWriter.startElement("label", this);

        responseWriter.writeText("Operator", null);

        responseWriter.endElement("label");

        responseWriter.startElement("select", this);

        //set name for decode in post request
        responseWriter.writeAttribute("name", CBOOPERATOR1, null);

        responseWriter.writeAttribute("class", "form-control input-sm", null);

        //responseWriter.writeAttribute("id", "operator1", null);
        //responseWriter.writeAttribute("name", "operator1", null);
        responseWriter.writeAttribute("size", "1", null);

        //load the operator1 list
        HtmlHelper.createHtmlSelectOptionsValue(responseWriter, this, this.internalStorage.get(CBOOPERATOR1), OPAND, true, true);

        responseWriter.endElement("select");
        responseWriter.endElement("div");
        responseWriter.endElement("div");

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "col-md-6", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "form-group", null);

        responseWriter.startElement("label", this);

        responseWriter.writeText("Value", null);

        responseWriter.endElement("label");
        responseWriter.startElement("input", this);
        responseWriter.writeAttribute("class", "form-control input-sm", null);

        //responseWriter.writeAttribute("id", "value1", null);
        //set name for decode in post request
        responseWriter.writeAttribute("name", TXTVALUE1, null);

        responseWriter.writeAttribute("type", "text", null);

        //create txtValue1
        HtmlHelper.createHtmlInputTypeTextValue(responseWriter, this, this.internalStorage.get(TXTVALUE1));

        responseWriter.endElement("input");
        responseWriter.endElement("div");
        responseWriter.endElement("div");
        responseWriter.endElement("div");
        responseWriter.startElement("h5", this);
        responseWriter.writeAttribute("class", "m-t-5 m-b-5 text-primary", null);

        responseWriter.writeAttribute("name", LBLAND, null);

        responseWriter.writeText("AND", null);

        responseWriter.endElement("h5");
        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "row", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "col-md-6", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "form-group", null);

        responseWriter.startElement("label", this);

        responseWriter.writeText("Operator", null);

        responseWriter.endElement("label");
        responseWriter.startElement("select", this);

        //set name for decode in post request
        responseWriter.writeAttribute("name", CBOOPERATOR2, null);

        responseWriter.writeAttribute("class", "form-control input-sm", null);

        //responseWriter.writeAttribute("id", "operator2", null);
        //responseWriter.writeAttribute("name", "operator2", null);
        responseWriter.writeAttribute("size", "1", null);

        //load the operator2 list
        HtmlHelper.createHtmlSelectOptionsValue(responseWriter, this, this.internalStorage.get(CBOOPERATOR2), OPAND, true, false);

        responseWriter.endElement("select");
        responseWriter.endElement("div");
        responseWriter.endElement("div");
        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "col-md-6", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "form-group", null);

        responseWriter.startElement("label", this);

        responseWriter.writeText("Value", null);

        responseWriter.endElement("label");
        responseWriter.startElement("input", this);
        responseWriter.writeAttribute("class", "netEmp form-control input-sm", null);

        //responseWriter.writeAttribute("id", "value2", null);
        //set name for decode in post request
        responseWriter.writeAttribute("name", TXTVALUE2, null);

        responseWriter.writeAttribute("type", "text", null);

        HtmlHelper.createHtmlInputTypeTextValue(responseWriter, this, this.internalStorage.get(TXTVALUE2));

        responseWriter.endElement("input");
        responseWriter.endElement("div");
        responseWriter.endElement("div");
        responseWriter.endElement("div");

    }

    public void decode(FacesContext context) {
        Map<String, String> requestMap
                = context.getExternalContext().getRequestParameterMap();
        String clientId = getClientId(context);

        //double check if button Save clicked
        if (requestMap.containsKey(clientId + BTN_SAVENAME)) {
            decodeFilter(requestMap, clientId);
            try {
                saveData();
            } catch (CustomComponentException ex) {
                Logger.getLogger(UINodeFilter.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

//        int increment;
//        if (requestMap.containsKey(clientId + MORE)) {
//            increment = 1;
//        } else if (requestMap.containsKey(clientId + LESS)) {
//            increment = -1;
//        } else {
//            increment = 0;
//        }
//
//        try {
//            int submittedValue
//                    = Integer.parseInt((String) requestMap.get(clientId));
//
//            int newValue = getIncrementedValue(submittedValue, increment);
//            setSubmittedValue("" + newValue);
//        } catch (NumberFormatException ex) {
//            // let the converter take care of bad input, but we still have 
//            // to set the submitted value, or the converter won't have 
//            // any input to deal with
//            setSubmittedValue((String) requestMap.get(clientId));
//        }
    }

    protected final String getChangeScript(FacesContext context, UIComponentBase component)
            throws IOException {
        return "loadValidatorRules();jsf.ajax.request('" + component.getParent().getClientId()
                + "', null, { 'render': 'nodesGroup' }); ";
    }

    @Override
    public String getFamily() {
        return "com.irealsoftware.karma.Nodes";
    }

    @Override
    public NodeTypeEnum getNodeType() {
        return NodeTypeEnum.FILTER;
    }

    @Override
    public void loadData() {

        loadListData();

        loadSavedData();
    }

    private void loadListData() {
        List<DataItemBase> tmpList;

        //load list values for cboFilterVar
        tmpList = this.daoFacade.NodeUtilGIEList(this.data.getId(), this.data.getEnvironmentId(), "A");

        //create key for cboFilterVar: store the list values property
        this.internalStorage.put(CBOFILTERVAR, new UINodeInternalData(tmpList));

        //load list values for cboOperator1
        tmpList = this.daoFacade.NodeUtilMethodList(this.data.getId(), this.data.getEnvironmentId());

        //remove AND operator
//        tmpList.removeIf((DataItemBase p) -> "AND".equals(p.getGuiName()));
//this.dataList = tmpList;
//create key for cboOperator1: store the list values property
        this.internalStorage.put(CBOOPERATOR1, new UINodeInternalData(tmpList));

//create key for txtValue1: store the list values property
        this.internalStorage.put(TXTVALUE1, new UINodeInternalData());

//create key for cboOperator1: store the list values property
        this.internalStorage.put(CBOOPERATOR2, new UINodeInternalData(tmpList));

//create key for txtValue1: store the list values property
        this.internalStorage.put(TXTVALUE2, new UINodeInternalData());
    }

    private void loadSavedData() {

        //load saved values and set te SelectedValue for each key (created in loadData)
        List<DataItemToken> tmpList = this.daoFacade.NodeUtilLoad_N(this.data.getId(), this.data.getEnvironmentId());

        //remove Empty guicontrolname
        tmpList.removeIf((DataItemToken p) -> p.getGuiControlName().isEmpty());

        for (DataItemToken item : tmpList) {
            String key = item.getGuiControlName();
            if (this.internalStorage.containsKey(key)) {
                this.internalStorage.get(key).setItemToken(item);
                this.internalStorage.get(key).setSelectedValue(item.getTokenText());
            }
        }
    }

    private void decodeFilter_old(Map<String, String> requestMap, String clientId) {

        requestMap.entrySet().stream().forEach(e -> {

            String key = e.getKey();
            String value = e.getValue();

            if (!Arrays.asList(fieldsLabel).contains(key)) {
                return;
            }

            if (value.length() == 0 || value.equalsIgnoreCase("none")) {
                return;
            }

            //create an DataItemToken instance based on key value from HTTPRequest post
            DataItemToken dit = createDataItemToken(key, value);

            //if exists set Selected Value
            if (this.internalStorage.containsKey(key)) {

                this.internalStorage.get(key).setSelectedValue(value);
                this.internalStorage.get(key).copyItemToken(dit);
            } //if doesn't exists put a new key value
            else {
                UINodeInternalData newNodeData = new UINodeInternalData();
                newNodeData.setSelectedValue(value);
                newNodeData.setItemToken(dit);

                this.internalStorage.put(e.getKey(), newNodeData);
            }
        });

        //create Dummy And operator if txtValue 2 is set
        createDummyANDOperator();
    }
    
        private void decodeFilter(Map<String, String> requestMap, String clientId) {

        requestMap.entrySet().stream().forEach(e -> {

            String key = e.getKey();
            String value = e.getValue();

            if (!Arrays.asList(fieldsLabel).contains(key)) {
                return;
            }

            if (value.length() == 0 || value.equalsIgnoreCase("none")) {
                return;
            }

            //create an DataItemToken instance based on key value from HTTPRequest post
            DataItemToken dit = createDataItemToken(key, value);

            //if exists set Selected Value
            if (this.internalStorage.containsKey(key)) {

                this.internalStorage.get(key).setSelectedValue(value);
                this.internalStorage.get(key).copyItemToken(dit);
            } //if doesn't exists put a new key value
            else {
                UINodeInternalData newNodeData = new UINodeInternalData();
                newNodeData.setSelectedValue(value);
                newNodeData.setItemToken(dit);

                this.internalStorage.put(e.getKey(), newNodeData);
            }
        });

        //create Dummy And operator if txtValue 2 is set
        createDummyANDOperator();
    }

    @Override
    public void saveData() throws CustomComponentException {

        //todo
        //must save in the order of selection: Var->Operator->txtValue
        //cred ca voi folosi o noua lista si voi adauga itemtoken in createFilterExpressionText dar redenumita
        try {
            //validate data to save
            validateData();

//        prepareSaveData();
//        //save expression
            String expressionText = createFilterExpressionText();
//
            //return if expression text is empty
            if (expressionText == null) {
                return;
            }
//
            int numGroupItemExpressionId = this.daoFacade.NodeSaveExpression(this.data.getId(), this.data.getEnvironmentId(), expressionText, 0, null);
            //delete token

            this.daoFacade.NodeUtilDeleteTokens(numGroupItemExpressionId, this.data.getEnvironmentId());
//        //save expressiontokenks
//        //Add(TokenType As String, TextValue As Variant, GUIControl As Variant, RefId As Variant, Optional sKey As String) As CToken
//
            this.internalStorage.entrySet().stream().forEach(e -> {

                DataItemToken dit = e.getValue().getItemToken();
                
                if (dit == null)
                    return;

                this.daoFacade.NodeSaveExpressionToken(this.data.getId(), this.data.getEnvironmentId(), numGroupItemExpressionId,
                        dit.getTokenSetName(), dit.getTokenText(), dit.getTokenRefId(), dit.getGuiControlName(), 0, null);
            }
            );
        } catch (Exception ex) {
            throw (new CustomComponentException(ex.getMessage(), ex));
//            Logger.getLogger(UINodeFilter.class.getName()).log(Level.SEVERE, null, ex);

        }
    }

    private String createFilterExpressionText() {

        //todo implemnt validation
        String result = null;
        DataItemToken dit = this.internalStorage.get(CBOFILTERVAR).getItemToken();

        if (dit == null) {
            return result;
        }

        String var = dit.getTokenText();

        if (var.isEmpty()) {
            return result;
        }

        dit = this.internalStorage.get(CBOOPERATOR1).getItemToken();

        if (dit == null) {
            return result;
        }

        String op = dit.getTokenText();

        if (op.isEmpty()) {
            return result;
        }

        dit = this.internalStorage.get(TXTVALUE1).getItemToken();

        if (dit == null) {
            return result;
        }

        String val = dit.getTokenText();

        if (val.isEmpty()) {
            return result;
        }

        result = var + op + val;

        dit = this.internalStorage.get(CBOOPERATOR2).getItemToken();

        if (dit == null) {
            return result;
        }

        op = dit.getTokenText();

        if (op.isEmpty()) {
            return result;
        }

        val = this.internalStorage.get(TXTVALUE2).getItemToken().getTokenText();

        if (val.isEmpty()) {
            return result;
        }

        result += " AND " + var + op + val;

        return result;
    }

    private void createDummyANDOperator() {
        //create a dummy for lblAND operator in order to save in database on NodeSaveExpressionToken

        DataItemToken dit = this.internalStorage.get(TXTVALUE2).getItemToken();

        if (dit == null) {
            return;
        }

        //verify if txtValue2 is set
        String val = dit.getTokenText();

        if (val.isEmpty()) {
            return;
        }

        UINodeInternalData nid = new UINodeInternalData();
        dit = new DataItemToken();
        DataItemBase dib = Iterables.find(this.internalStorage.get(CBOOPERATOR1).getListData(), new Predicate<DataItemBase>() {
            @Override
            public boolean apply(DataItemBase input) {
                return "AND".equals(input.getOperationalName());
            }
        });

        Integer andId = dib.getId();

        dit.setTokenSetName("OPERATOR");
        dit.setTokenText("AND");
        dit.setTokenRefId(andId);
        dit.setGuiControlName(LBLAND);

        nid.setItemToken(dit);

        this.internalStorage.put(LBLAND, nid);
    }

    /**
     * @Description: CReate DatItemToken based on key, value from Request
     * parameters
     *
     */
    private DataItemToken createDataItemToken(String key, String value) {

        DataItemToken result = new DataItemToken();
        try {
            int refId;
            //split into 2 items array string separated by semicolon
            Entry<Integer, String> entry = StringHelper.getKeyValueFromParsedString(value, ";");
            refId = entry.getKey();
            result.setTokenRefId(refId);
            result.setTokenText(entry.getValue());
            result.setGuiControlName(key);

            TokenTypeEnum tte = convertKey2TokenType(key);

            result.setTokenType(tte);

            if (tte == TokenTypeEnum.UNDEFINED) {
                throw (new NotSupportedException());
            }
            result.setTokenSetName(tte.toString());

        } catch (NotSupportedException ex) {
            Logger.getLogger(UINodeFilter.class.getName()).log(Level.SEVERE, null, ex);
        }

        return result;
    }

    private TokenTypeEnum convertKey2TokenType(String key) {
        switch (key) {
            case CBOFILTERVAR:
                return TokenTypeEnum.VARIABLE;

            case CBOOPERATOR1:
            case CBOOPERATOR2:
                return TokenTypeEnum.OPERATOR;

            case TXTVALUE1:
            case TXTVALUE2:
                return TokenTypeEnum.VALUE;
            default:
                return TokenTypeEnum.UNDEFINED;

        }
    }

    private void validateData() {
        //todo
    }

//    private List<DataItemToken> prepareSaveData() {
//
//        List<DataItemToken> result = new ArrayList<DataItemToken>();
//
//        this.internalStorage.entrySet().stream().forEach(e -> {
//
//            DataItemToken dit = e.getValue().getItemToken();
//            
//                    String var = this.internalStorage.get(CBOFILTERVAR).getItemToken().getTokenText();
//
//        if (var.isEmpty()) {
//            return null;
//        }
//
//        String op = this.internalStorage.get(CBOOPERATOR1).getItemToken().getTokenText();
//
//        String val = this.internalStorage.get(TXTVALUE1).getItemToken().getTokenText();
//
//        if (op.isEmpty() || val.isnetEmpty()) {
//            return null;
//        }
//
//        result = var + op + val;
//
//        op = this.internalStorage.get(CBOOPERATOR2).getItemToken().getTokenText();
//
//        val = this.internalStorage.get(TXTVALUE2).getItemToken().getTokenText();
//
//        if (op.isEmpty() || val.isEmpty()) {
//            return result;
//        }
//
//        }
//        );
//    }
    private Object getItemfromListData(String key, String operationalName) {

        Object result = null;

        List<DataItemBase> list = this.internalStorage.get(key).getListData();

        if (list == null) {
            return result;
        }

        DataItemBase dib = Iterables.find(list, new Predicate<DataItemBase>() {
            @Override
            public boolean apply(DataItemBase input) {
                return operationalName.equals(input.getOperationalName());
            }
        });

        result = dib.getId();

        return result;
    }
}
