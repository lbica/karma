/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.irealsoftware.karma.customcomponents;

import com.irealsoftware.karma.entities.NodeTypeEnum;
import com.irealsoftware.karma.entities.StrategyNode;
import com.irealsoftware.karma.facade.DAOFacade;
import java.io.IOException;
import java.util.Map;
import javax.faces.component.FacesComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;

/**
 *
 * @author Laurentiu
 */
@FacesComponent("com.irealsoftware.karma.NodeAssign")
public class UINodeAssign extends AbstractUINodeComponent<StrategyNode>{

    public UINodeAssign(StrategyNode data, DAOFacade dao) {
        super(data, dao);
//        setRendererType(null);
    }

    @Override
    public String getFamily() {
        return "com.irealsoftware.karma.Nodes";
    }

    @Override
    public NodeTypeEnum getNodeType() {
        return NodeTypeEnum.ASSIGN;
    }

    public void encodeBegin(FacesContext context) throws IOException {

        ResponseWriter responseWriter = context.getResponseWriter();
        String clientId = getClientId(context);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "form-group", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "input-group", null);

        responseWriter.startElement("input", this);
        responseWriter.writeAttribute("class", "form-control input-sm", null);
        
        responseWriter.writeAttribute("readonly", "readonly", null);

        responseWriter.writeAttribute("value", this.getName(), null);

        responseWriter.endElement("input");
        responseWriter.startElement("span", this);
        responseWriter.writeAttribute("class", "input-group-btn", null);

        responseWriter.startElement("button", this);
        responseWriter.writeAttribute("class", "btn btn-primary btn-sm", null);

        responseWriter.writeAttribute("type", "button", null);

        responseWriter.writeText("Save", null);

        responseWriter.endElement("button");
        responseWriter.endElement("span");
        responseWriter.endElement("div");
        responseWriter.endElement("div");
        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "form-group", null);

        responseWriter.startElement("label", this);

        responseWriter.writeText("Variable", null);

        responseWriter.endElement("label");
        responseWriter.startElement("select", this);
        responseWriter.writeAttribute("class", "form-control input-sm", null);

        responseWriter.writeAttribute("id", "direction", null);

        responseWriter.writeAttribute("name", "variable", null);

        responseWriter.writeAttribute("size", "1", null);

        responseWriter.startElement("option", this);
        responseWriter.writeAttribute("value", "Opt1", null);

        responseWriter.writeText("Opt1", null);

        responseWriter.endElement("option");
        responseWriter.startElement("option", this);
        responseWriter.writeAttribute("value", "Opt2", null);

        responseWriter.writeText("Opt2", null);

        responseWriter.endElement("option");
        responseWriter.endElement("select");
        responseWriter.endElement("div");
        responseWriter.startElement("fieldset", this);

        responseWriter.startElement("legend", this);

        responseWriter.writeText("Assign To:", null);

        responseWriter.endElement("legend");
        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "row", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "col-md-4", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "form-group", null);

        responseWriter.startElement("label", this);
        responseWriter.writeAttribute("class", "radio radio-inline m-5", null);

        responseWriter.startElement("input", this);

        responseWriter.writeAttribute("id", "assign_to", null);

        responseWriter.writeAttribute("name", "constant", null);

        responseWriter.writeAttribute("type", "radio", null);

        responseWriter.writeAttribute("value", "1", null);

        responseWriter.endElement("input");
        responseWriter.startElement("span", this);
        responseWriter.writeAttribute("class", "lbl", null);

        responseWriter.writeText("Constant", null);

        responseWriter.endElement("span");
        responseWriter.endElement("label");
        responseWriter.endElement("div");
        responseWriter.endElement("div");
        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "col-md-8", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "form-group", null);

        responseWriter.startElement("input", this);
        responseWriter.writeAttribute("class", "form-control input-sm", null);

        responseWriter.writeAttribute("id", "value1", null);

        responseWriter.writeAttribute("name", "value1", null);

        responseWriter.writeAttribute("type", "text", null);

        responseWriter.writeAttribute("value", "7", null);

        responseWriter.endElement("input");
        responseWriter.endElement("div");
        responseWriter.endElement("div");
        responseWriter.endElement("div");
        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "row", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "col-md-4 p-r-0", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "form-group", null);

        responseWriter.startElement("label", this);
        responseWriter.writeAttribute("class", "radio radio-inline m-5", null);

        responseWriter.startElement("input", this);
        responseWriter.writeAttribute("id", "assign_to", null);

        responseWriter.writeAttribute("name", "constant", null);

        responseWriter.writeAttribute("type", "radio", null);

        responseWriter.writeAttribute("value", "1", null);

        responseWriter.endElement("input");
        responseWriter.startElement("span", this);
        responseWriter.writeAttribute("class", "lbl", null);

        responseWriter.writeText("Formula", null);

        responseWriter.endElement("span");
        responseWriter.endElement("label");
        responseWriter.endElement("div");
        responseWriter.endElement("div");
        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "col-md-8", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "form-group", null);

        responseWriter.startElement("input", this);
        responseWriter.writeAttribute("class", "form-control input-sm", null);

        responseWriter.writeAttribute("id", "value1", null);

        responseWriter.writeAttribute("name", "value1", null);

        responseWriter.writeAttribute("type", "text", null);

        responseWriter.writeAttribute("value", "7", null);

        responseWriter.endElement("input");
        responseWriter.endElement("div");
        responseWriter.endElement("div");
        responseWriter.endElement("div");
        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "row", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "col-md-4 p-r-0", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "text-right text-primary m-t-5", null);

        responseWriter.startElement("i", this);
        responseWriter.writeAttribute("class", "zmdi zmdi-close zmdi-hc-fw zmdi-hc-lg", null);

        responseWriter.endElement("i");
        responseWriter.endElement("div");
        responseWriter.endElement("div");
        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "col-md-8", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "form-group", null);

        responseWriter.startElement("input", this);
        responseWriter.writeAttribute("class", "form-control input-sm", null);

        responseWriter.writeAttribute("id", "value1", null);

        responseWriter.writeAttribute("name", "value1", null);

        responseWriter.writeAttribute("type", "text", null);

        responseWriter.writeAttribute("value", "7", null);

        responseWriter.endElement("input");
        responseWriter.endElement("div");
        responseWriter.endElement("div");
        responseWriter.endElement("div");
        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "row", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "col-md-4 p-r-0", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "text-right text-primary m-t-5", null);

        responseWriter.startElement("i", this);
        responseWriter.writeAttribute("class", "zmdi zmdi-plus zmdi-hc-fw zmdi-hc-lg", null);

        responseWriter.endElement("i");
        responseWriter.endElement("div");
        responseWriter.endElement("div");
        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "col-md-8", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "form-group", null);

        responseWriter.startElement("input", this);
        responseWriter.writeAttribute("class", "form-control input-sm", null);

        responseWriter.writeAttribute("id", "value1", null);

        responseWriter.writeAttribute("name", "value1", null);

        responseWriter.writeAttribute("type", "text", null);

        responseWriter.writeAttribute("value", "7", null);

        responseWriter.endElement("input");
        responseWriter.endElement("div");
        responseWriter.endElement("div");
        responseWriter.endElement("div");
        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "row", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "col-md-4", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "form-group", null);

        responseWriter.startElement("label", this);
        responseWriter.writeAttribute("class", "radio radio-inline m-5", null);

        responseWriter.startElement("input", this);
        responseWriter.writeAttribute("id", "assign_to", null);

        responseWriter.writeAttribute("name", "constant", null);

        responseWriter.writeAttribute("type", "radio", null);

        responseWriter.writeAttribute("value", "1", null);

        responseWriter.endElement("input");
        responseWriter.startElement("span", this);
        responseWriter.writeAttribute("class", "lbl", null);

        responseWriter.writeText("Null", null);

        responseWriter.endElement("span");
        responseWriter.endElement("label");
        responseWriter.endElement("div");
        responseWriter.endElement("div");
        responseWriter.endElement("div");
        responseWriter.endElement("fieldset");
    }

    public void decode(FacesContext context) {
        Map<String, String> requestMap
                = context.getExternalContext().getRequestParameterMap();
        String clientId = getClientId(context);
    }

    @Override
    public void loadData() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private void loadSavedData() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void saveData() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
