/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.irealsoftware.karma.customcomponents;

import com.irealsoftware.karma.entities.NodeTypeEnum;
import com.irealsoftware.karma.entities.StrategyNode;
import com.irealsoftware.karma.facade.DAOFacade;
import java.io.IOException;
import java.util.Map;
import javax.faces.component.FacesComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;

/**
 *
 * @author Laurentiu
 */
@FacesComponent("com.irealsoftware.karma.NodeFilterOther")
public class UINodeFilterOther extends AbstractUINodeComponent<StrategyNode> {

    public UINodeFilterOther(StrategyNode data, DAOFacade dao) {
        super(data, dao);
//        setRendererType(null);
    }

    public void encodeBegin(FacesContext context) throws IOException {
        ResponseWriter responseWriter = context.getResponseWriter();
        String clientId = getClientId(context);
        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "form-group", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "input-group", null);

        responseWriter.startElement("input", this);
        responseWriter.writeAttribute("class", "form-control input-sm", null);

        responseWriter.writeAttribute("readonly", "readonly", null);
        responseWriter.writeAttribute("value", this.getName(), null);

        responseWriter.endElement("input");
        responseWriter.startElement("span", this);
        responseWriter.writeAttribute("class", "input-group-btn", null);

        responseWriter.startElement("button", this);
        responseWriter.writeAttribute("class", "btn btn-primary btn-sm disabled", null);

        responseWriter.writeAttribute("type", "button", null);

        responseWriter.writeText("Save", null);

        responseWriter.endElement("button");
        responseWriter.endElement("span");
        responseWriter.endElement("div");
        responseWriter.endElement("div");

    }

    public void decode(FacesContext context) {
        Map<String, String> requestMap
                = context.getExternalContext().getRequestParameterMap();
        String clientId = getClientId(context);

//        int increment;
//        if (requestMap.containsKey(clientId + MORE)) {
//            increment = 1;
//        } else if (requestMap.containsKey(clientId + LESS)) {
//            increment = -1;
//        } else {
//            increment = 0;
//        }
//
//        try {
//            int submittedValue
//                    = Integer.parseInt((String) requestMap.get(clientId));
//
//            int newValue = getIncrementedValue(submittedValue, increment);
//            setSubmittedValue("" + newValue);
//        } catch (NumberFormatException ex) {
//            // let the converter take care of bad input, but we still have 
//            // to set the submitted value, or the converter won't have 
//            // any input to deal with
//            setSubmittedValue((String) requestMap.get(clientId));
//        }
    }

    @Override
    public String getFamily() {
        return "com.irealsoftware.karma.Nodes";
    }

    @Override
    public NodeTypeEnum getNodeType() {
        return NodeTypeEnum.FILTER;
    }

    @Override
    public void loadData() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    private void loadSavedData() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void saveData() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
