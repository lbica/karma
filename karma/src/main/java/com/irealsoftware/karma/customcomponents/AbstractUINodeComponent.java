/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.irealsoftware.karma.customcomponents;

import com.irealsoftware.karma.entities.UINodeInternalData;
import com.irealsoftware.karma.entities.NodeTypeEnum;
import com.irealsoftware.karma.exceptions.CustomComponentException;
import com.irealsoftware.karma.facade.DAOFacade;
import java.io.IOException;
import java.util.HashMap;
import javax.faces.component.UIComponentBase;
import javax.faces.context.FacesContext;

/**
 *
 * @author Laurentiu
 */


public abstract class AbstractUINodeComponent<T> extends UIComponentBase {
    
    protected static final String BTN_SAVENAME = "btnSaveNode";
    
    protected Integer nodeId;

    protected DAOFacade daoFacade;

    protected T data;

//    protected List<DataItemBase> dataList;
    protected HashMap<String, UINodeInternalData> internalStorage;

    protected AbstractUINodeComponent(T data, DAOFacade dao) {

        daoFacade = dao;

        internalStorage = new HashMap<String, UINodeInternalData>();

        this.data = data;

        this.loadData();

//        this.setSelectedValues();

        setRendererType(null);

    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String saveProcedure;

    private NodeTypeEnum nodeType;

    public NodeTypeEnum getNodeType() {
        return nodeType;
    }

    public String getSaveProcedure() {
        return saveProcedure;
    }

    public void setSaveProcedure(String saveProcedure) {
        this.saveProcedure = saveProcedure;
    }



    
    public void encodeBegin(FacesContext context) throws IOException {
        //
    }
    
    public abstract void loadData();
    
    public abstract void saveData() throws CustomComponentException;


}
