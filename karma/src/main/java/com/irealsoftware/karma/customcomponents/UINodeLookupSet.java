/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.irealsoftware.karma.customcomponents;

import com.irealsoftware.karma.entities.NodeTypeEnum;
import com.irealsoftware.karma.entities.StrategyNode;
import com.irealsoftware.karma.facade.DAOFacade;
import java.io.IOException;
import java.util.Map;
import javax.faces.component.FacesComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;

/**
 *
 * @author Laurentiu
 */
@FacesComponent("com.irealsoftware.karma.NodeLookupSet")
public class UINodeLookupSet extends AbstractUINodeComponent<StrategyNode> {

    public UINodeLookupSet(StrategyNode data, DAOFacade dao) {
        super(data, dao);
//        setRendererType(null);
    }

    public void encodeBegin(FacesContext context) throws IOException {
        ResponseWriter responseWriter = context.getResponseWriter();
        String clientId = getClientId(context);
        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "form-group", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "input-group", null);

        responseWriter.startElement("input", this);
        responseWriter.writeAttribute("class", "form-control input-sm", null);

        responseWriter.writeAttribute("readonly", "readonly", null);
        responseWriter.writeAttribute("value", this.getName(), null);

        responseWriter.endElement("input");
        responseWriter.startElement("span", this);
        responseWriter.writeAttribute("class", "input-group-btn", null);

        responseWriter.startElement("button", this);
        responseWriter.writeAttribute("class", "btn btn-primary btn-sm", null);

        responseWriter.writeAttribute("type", "button", null);

        responseWriter.writeText("Save", null);

        responseWriter.endElement("button");
        responseWriter.endElement("span");
        responseWriter.endElement("div");
        responseWriter.endElement("div");
        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "form-group", null);

        responseWriter.startElement("label", this);

        responseWriter.writeText("LookUp Table", null);

        responseWriter.endElement("label");
        responseWriter.startElement("select", this);
        responseWriter.writeAttribute("class", "form-control input-sm", null);

        responseWriter.writeAttribute("id", "lookup_table", null);

        responseWriter.writeAttribute("name", "lookup_table", null);

        responseWriter.writeAttribute("size", "1", null);

        responseWriter.startElement("option", this);
        responseWriter.writeAttribute("value", "Collection entity lookup", null);

        responseWriter.writeText("Collection entity lookup", null);

        responseWriter.endElement("option");
        responseWriter.startElement("option", this);
        responseWriter.writeAttribute("value", "Opt2", null);

        responseWriter.writeText("Opt2", null);

        responseWriter.endElement("option");
        responseWriter.endElement("select");
        responseWriter.endElement("div");
        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "form-group", null);

        responseWriter.startElement("label", this);

        responseWriter.writeText("LookUp Input", null);

        responseWriter.endElement("label");
        responseWriter.startElement("select", this);
        responseWriter.writeAttribute("class", "form-control input-sm", null);

        responseWriter.writeAttribute("id", "lookup_input", null);

        responseWriter.writeAttribute("name", "lookup_input", null);

        responseWriter.writeAttribute("size", "1", null);

        responseWriter.startElement("option", this);
        responseWriter.writeAttribute("value", "Opt1", null);

        responseWriter.writeText("Opt1", null);

        responseWriter.endElement("option");
        responseWriter.startElement("option", this);
        responseWriter.writeAttribute("value", "Opt2", null);

        responseWriter.writeText("Opt2", null);

        responseWriter.endElement("option");
        responseWriter.endElement("select");
        responseWriter.endElement("div");
        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "form-group", null);

        responseWriter.startElement("label", this);

        responseWriter.writeText("LookUp Filter", null);

        responseWriter.endElement("label");
        responseWriter.startElement("select", this);
        responseWriter.writeAttribute("class", "form-control input-sm", null);

        responseWriter.writeAttribute("id", "lookup_filter", null);

        responseWriter.writeAttribute("name", "lookup_filter", null);

        responseWriter.writeAttribute("size", "1", null);

        responseWriter.startElement("option", this);
        responseWriter.writeAttribute("value", "Challenger1", null);

        responseWriter.writeText("Challenger1", null);

        responseWriter.endElement("option");
        responseWriter.startElement("option", this);
        responseWriter.writeAttribute("value", "Opt2", null);

        responseWriter.writeText("Opt2", null);

        responseWriter.endElement("option");
        responseWriter.endElement("select");
        responseWriter.endElement("div");
        responseWriter.startElement("fieldset", this);

        responseWriter.startElement("legend", this);

        responseWriter.writeText("Look-Up Set", null);

        responseWriter.endElement("legend");
        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "row", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "col-md-4", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "form-group", null);

        responseWriter.startElement("label", this);
        responseWriter.writeAttribute("class", "radio radio-inline m-5", null);

        responseWriter.startElement("input", this);

        responseWriter.writeAttribute("id", "assign_to", null);

        responseWriter.writeAttribute("name", "constant", null);

        responseWriter.writeAttribute("type", "radio", null);

        responseWriter.writeAttribute("value", "1", null);

        responseWriter.endElement("input");
        responseWriter.startElement("span", this);
        responseWriter.writeAttribute("class", "lbl", null);

        responseWriter.writeText("Variable", null);

        responseWriter.endElement("span");
        responseWriter.endElement("label");
        responseWriter.endElement("div");
        responseWriter.endElement("div");
        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "col-md-8", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "form-group", null);

        responseWriter.startElement("select", this);
        responseWriter.writeAttribute("class", "form-control input-sm", null);

        responseWriter.writeAttribute("id", "lookup_filter", null);

        responseWriter.writeAttribute("name", "lookup_filter", null);

        responseWriter.writeAttribute("size", "1", null);

        responseWriter.startElement("option", this);
        responseWriter.writeAttribute("value", "Opt1", null);

        responseWriter.writeText("Opt1", null);

        responseWriter.endElement("option");
        responseWriter.startElement("option", this);
        responseWriter.writeAttribute("value", "Opt2", null);

        responseWriter.writeText("Opt2", null);

        responseWriter.endElement("option");
        responseWriter.endElement("select");
        responseWriter.endElement("div");
        responseWriter.endElement("div");
        responseWriter.endElement("div");
        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "row", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "col-md-4 p-r-0", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "form-group", null);

        responseWriter.startElement("label", this);
        responseWriter.writeAttribute("class", "radio radio-inline m-5", null);

        responseWriter.startElement("input", this);
        responseWriter.writeAttribute("id", "assign_to", null);

        responseWriter.writeAttribute("name", "constant", null);

        responseWriter.writeAttribute("type", "radio", null);

        responseWriter.writeAttribute("value", "1", null);

        responseWriter.endElement("input");
        responseWriter.startElement("span", this);
        responseWriter.writeAttribute("class", "lbl", null);

        responseWriter.writeText("Value", null);

        responseWriter.endElement("span");
        responseWriter.endElement("label");
        responseWriter.endElement("div");
        responseWriter.endElement("div");
        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "col-md-8", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "form-group", null);

        responseWriter.startElement("input", this);
        responseWriter.writeAttribute("class", "form-control input-sm", null);

        responseWriter.writeAttribute("id", "value1", null);

        responseWriter.writeAttribute("name", "value1", null);

        responseWriter.writeAttribute("type", "text", null);

        responseWriter.writeAttribute("value", "7", null);

        responseWriter.endElement("input");
        responseWriter.endElement("div");
        responseWriter.endElement("div");
        responseWriter.endElement("div");
        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "row", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "col-md-4", null);

        responseWriter.startElement("div", this);
        responseWriter.writeAttribute("class", "form-group", null);

        responseWriter.startElement("label", this);
        responseWriter.writeAttribute("class", "radio radio-inline m-5", null);

        responseWriter.startElement("input", this);
        responseWriter.writeAttribute("id", "assign_to", null);

        responseWriter.writeAttribute("name", "constant", null);

        responseWriter.writeAttribute("type", "radio", null);

        responseWriter.writeAttribute("value", "1", null);

        responseWriter.endElement("input");
        responseWriter.startElement("span", this);
        responseWriter.writeAttribute("class", "lbl", null);

        responseWriter.writeText("Delete", null);

        responseWriter.endElement("span");
        responseWriter.endElement("label");
        responseWriter.endElement("div");
        responseWriter.endElement("div");
        responseWriter.endElement("div");
        responseWriter.endElement("fieldset");

    }

    public void decode(FacesContext context) {
        Map<String, String> requestMap
                = context.getExternalContext().getRequestParameterMap();
        String clientId = getClientId(context);

//        int increment;
//        if (requestMap.containsKey(clientId + MORE)) {
//            increment = 1;
//        } else if (requestMap.containsKey(clientId + LESS)) {
//            increment = -1;
//        } else {
//            increment = 0;
//        }
//
//        try {
//            int submittedValue
//                    = Integer.parseInt((String) requestMap.get(clientId));
//
//            int newValue = getIncrementedValue(submittedValue, increment);
//            setSubmittedValue("" + newValue);
//        } catch (NumberFormatException ex) {
//            // let the converter take care of bad input, but we still have 
//            // to set the submitted value, or the converter won't have 
//            // any input to deal with
//            setSubmittedValue((String) requestMap.get(clientId));
//        }
    }

    @Override
    public String getFamily() {
        return "com.irealsoftware.karma.Nodes";
    }

    @Override
    public NodeTypeEnum getNodeType() {
        return NodeTypeEnum.LOOKUPSET;
    }

    @Override
    public void loadData() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }


    private void loadSavedData() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void saveData() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
