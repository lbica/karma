//this means document ready
$(function () {
    //alert('test load');
    // validate the comment form when it is submitted


//    $('.netEmp').each(function () {
//        $(this).rules("add", {
//            required: true
//        });
//    });
//
//    $("#frmMain").validate();

    // validate signup form on keyup and submit
//    $("#placeHolder").validate({
//        rules: {
//            firstname: "required",
//            lastname: "required"
//        },
//        messages: {
//            firstname: "Please enter your firstname",
//            lastname: "Please enter your lastname"
//        }
//    });


});




function getStategiesByGroupEnv(groupId, envId) {

    var data = {groupId: groupId, environmentId: envId};
    //alert(JSON.stringify(data));

    $.ajax({
        type: "POST",
        url: "/karma/groups/strategies",
        data: JSON.stringify(data),
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        async: true,
        success: function (data)
        {

            $("#ulstrategies").empty();
            var strHeader = '<li class="list-group-item">';
            strHeader += '<ul class="actions pull-right">';
            strHeader += '<li><a href=""><i class="zmdi zmdi-play"></i></a></li>';
            strHeader += '<li><a href=""><i class="zmdi zmdi-edit"></i></a></li>';
            strHeader += '<li><a href=""><i class="zmdi zmdi-refresh"></i></a></li>';
            var item;
            $.each(data, function (key, value) {


                item = strHeader + '</ul><a href="#">' + value.guiname + '</a></li>';
                $("#ulstrategies").append(item);
                item = '';
            })
        },
        error: function (e)
        {
            alert("ERROR: ", e);
        }
    });
}

/*
 * load validator rules
 */

function loadValidatorRules() {

    //alert('test');

    $.validator.addMethod('notSelected', function (value, element) {
        return (value != 'none');
    }, 'Please select a value.');




    $("#frmMain").validate({
        rules: {
            cboFilterVar: {
                notSelected: true
            },
            cboOperator1: {
                notSelected: true
            },
            txtValue1: {
                required: function (element) {
                    return $("[name='cboOperator1']").val() != 'none';
                }
            },
            txtValue2: {
                required: function (element) {
                    return $("[name='cboOperator2']").val() != 'none';
                }
            },

            password: {
                required: true,
                minlength: 5
            },
            confirm_password: {
                required: true,
                minlength: 5,
                equalTo: "#password"
            }
        },
        messages: {
            txtValue1: "Please enter a valid value for operator 1",
            txtValue2: "Please enter a valid value for operator 2",
            username: {
                required: "Please enter a username",
                minlength: "Your username must consist of at least 2 characters"
            },
            password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long"
            },
            confirm_password: {
                required: "Please provide a password",
                minlength: "Your password must be at least 5 characters long",
                equalTo: "Please enter the same password as above"
            }
        },
        errorElement: "em",
        errorPlacement: function (error, element) {
            // Add the `help-block` class to the error element
            error.addClass("help-block");

            if (element.prop("type") === "checkbox") {
                error.insertAfter(element.parent("label"));
            } else {
                error.insertAfter(element);
            }
        },
        highlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-error").removeClass("has-success");
        },
        unhighlight: function (element, errorClass, validClass) {
            $(element).parents(".col-sm-5").addClass("has-success").removeClass("has-error");
        }
    });

}



